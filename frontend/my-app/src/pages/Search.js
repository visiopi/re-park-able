import { Col, Container, Row, Tab, Tabs } from "react-bootstrap";
import { useLocation } from "react-router-dom";

import CampgroundCard from "../Cards/CampgroundCard";
import TrailCard from "../Cards/TrailCard";
import ParkCard from "../Cards/ParkCard";

import axios from "axios";
import { useEffect, useState } from "react";

const Search = () => {
	const [data, setData] = useState([]);
	const [loaded, setLoaded] = useState(false);

	const loc = useLocation();
	const query = loc.pathname.split("/search/").at(-1);
	const queryReg = new RegExp(`(?:${query.replaceAll("%20", "|")})`, "i");

	const client = axios.create({
		baseURL: "https://api.re-park-able.me/search/all/",
	});

	const getResult = async () => {
		await client
			.get(query)
			.then((response) => {
			console.log(response.data);
			setData(response.data);
			setLoaded(true);
			console.log(response);
			})
			.catch((err) => console.log(err));
	};

	useEffect(() => {
		getResult();
		console.log(data);
	  }, []);

	return (
		<Container>
		<br></br>
			<Row>
			<h4>Parks</h4>
			<br></br>
			{loaded ? (
              data["parks"].map((park) => (
                <Col key={park.name}>
                  <ParkCard park={park} regex={queryReg}/>
                </Col>
              ))
            ) : (
              <h1></h1>
            )}
			</Row>
			<Row>
				<br></br>
				<h4>Campgrounds</h4>
				<br></br>
				{loaded ? (
              data["campgrounds"].map((campground) => (
                <Col key={campground.name}>
                  <CampgroundCard campground={campground} regex={queryReg}/>
                </Col>
              ))
				) : (
				<h1></h1>
				)}
			</Row>
			<Row>
				<br></br>
				<h4>Trails</h4>
				<br></br>
				{loaded ? (
              data["trails"].map((trail) => (
                <Col key={trail.title}>
                  <TrailCard trail={trail} regex={queryReg}/>
                </Col>
              ))
            ) : (
              <h1></h1>
            )}
			</Row>
		</Container>
	);
	
};

export default Search;


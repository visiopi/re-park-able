from unittest import main, TestCase
from app import app


class TestModels(TestCase):
    def test_parks(self):
        test_client = app.test_client()
        req = test_client.get("/parks?page=1&perPage=1")
        self.assertEqual(req.status_code, 200)

    def test_parks_by_id(self):
        test_client = app.test_client()
        req = test_client.get("/parks?page=2")
        self.assertEqual(req.status_code, 200)

    def test_trails(self):
        test_client = app.test_client()
        req = test_client.get("trails?page=3")
        self.assertEqual(req.status_code, 200)

    def test_trails_by_id(self):
        test_client = app.test_client()
        req = test_client.get("trail/019C3D0F-0AD1-4A1B-8E5F-601A65918303")
        self.assertEqual(req.status_code, 200)

    def test_campgrounds(self):
        test_client = app.test_client()
        req = test_client.get("/campgrounds?page=1&perPage=1")
        self.assertEqual(req.status_code, 200)

    def test_campgrounds_by_id(self):
        test_client = app.test_client()
        req = test_client.get("campground/66DA0C26-4D6E-4C02-AA89-794655DF2C38")
        self.assertEqual(req.status_code, 200)


if __name__ == "__main__":
    main()

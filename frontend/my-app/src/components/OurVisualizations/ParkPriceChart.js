import React from 'react';
import { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import axios from "axios";

const ParkPriceChart = () => {
    const [loading, setLoading] = useState(true);
    const [localData, setLocalData] = useState([]);

    const getData = async() => {
        const STATES = [
            'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID',
            'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS',
            'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK',
            'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV',
            'WI', 'WY'
        ];
        const baseURL = "https://api.re-park-able.me/parks";
        let innerData = []
        for(let i = 0; i<STATES.length; i++) {
            let state = STATES[i];
            var query = `?state=` + state;
            let newURL = baseURL + query;
            const response = await axios.get(newURL);
            let responseData = response.data.data;
            var mostExpensiveFee = -1;
            for (let j = 0; j<responseData.length; j++){
                let parkDetails = responseData[j];
                let fee = parkDetails["entranceFee"];
                if (fee > mostExpensiveFee) {
                    mostExpensiveFee = fee;
                }
            }
            if (mostExpensiveFee == -1) {
                mostExpensiveFee = 0;
            }
            innerData.push({name: state, mostExpensive: mostExpensiveFee});
        }
        setLocalData(innerData);
        setLoading(false);
    }

    useEffect(() => {
        getData();
    }, []);

    return (
        <div>
        {loading && <p>Loading...</p>}
        {!loading && <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Cost of Most Expensive Park In Each State</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                    <BarChart
                        height={300}
                        data={localData}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="mostExpensive" fill="#977FD7" unit="$"/>
                    </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>}
        </div>
    );
}

export default ParkPriceChart;


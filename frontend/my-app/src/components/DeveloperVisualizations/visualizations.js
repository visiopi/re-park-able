import RestaurantPriceRatingNY from '../DeveloperVisualizations/restaurant_price_rating_ny';
import EventsClassification from './events_classification';
import InOutTuition from './in_out_tx_tuition'

const DevVisualizations = () => {
    return (
        <div>
            <RestaurantPriceRatingNY /> 
            <EventsClassification />
            <InOutTuition /> 
        </div>
    );
}

export default DevVisualizations;
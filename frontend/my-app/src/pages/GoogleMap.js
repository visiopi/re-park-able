import React from "react";
import "./PagesFormatting.css"

const GoogleMap = ({place, name}) => {
    if (place.longitude === "" || place.latitude === "") {
        return (
            <p>
                There is no Google Maps data available.
            </p>
        )
    }

    var longitude = +place.longitude;
    var latitude = +place.latitude;
    var tokenizedPlaceLocation = name.split(' ');
    var urlEscapedPlaceLocation = "";

    var i;
    for (i = 0; i < tokenizedPlaceLocation.length - 1; i++) {
        var stringToAppend = tokenizedPlaceLocation[i] + "+";
        urlEscapedPlaceLocation += stringToAppend;
    }
    urlEscapedPlaceLocation += tokenizedPlaceLocation[i];

    const API_KEY = process.env.REACT_APP_GOOGLE_API_KEY;

    var googleMapSrcUrl = "https://www.google.com/maps/embed/v1/place?key=" + API_KEY + "&q=" 
                            + urlEscapedPlaceLocation + "&center=" + latitude + "," + longitude;

    return (
        <div className='googleMapsAPI'>
            <h5>
                Map:  
            </h5>
            <iframe //need to conditionally create google map if there's a valid longitude and latitude!!*****
                width="450"
                height="250"
                src={googleMapSrcUrl}
                allowfullscreen>
            </iframe>
        </div>
    )
}

export default GoogleMap
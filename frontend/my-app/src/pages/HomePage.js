import './HomePage.css';
import Card from 'react-bootstrap/Card';
import Image from 'react-bootstrap/Image'
import CardGroup from 'react-bootstrap/CardGroup';
import {Link } from "react-router-dom";
import styled from "styled-components";

import Button from 'react-bootstrap/Button';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Stack from 'react-bootstrap/Stack'

const LinkButton = styled.button`
	border-radius: .5rem;
	color:  #045901;
	margin: 10px;
	background-color: white;
	outline: 1px #045901;
	&:hover {
    	background-color: #045901;
		color: white;
  	}
`;

function HomePage() {
	// document.body.style = 'background: black;';
	return (
		<>
		{/* Header */}
		<Stack>
		<div className="App" id="root">
				{/* Header Image */}
				{/* <Image width="100%"
				src= "https://www.nps.gov/npgallery/GetAsset/729F481A-1DD8-B71B-0B226D408AB3D737/proxy/hires"
				rounded /> */}
				{/* <div class="App-title"><h1>Re-Park-Able</h1></div>
				<div class="App-line"></div>
				<div class="App-Description">
					<p>Find remarkable parks</p>
				</div> */}
				<br></br><br></br>
				<h1> Re-Park-Able</h1>
				<h4>Find Remarkable Parks</h4>
				<br></br>
				{/* Cards */}
				<img src="https://images.pexels.com/photos/1867601/pexels-photo-1867601.jpeg?auto=compress&cs=tinysrgb&w=800" width='1000rem'></img>
				
				
				
				<Container className="p-4">
				<br></br>
                <h4>Plan your next adventure in the great outdoors</h4>
				<br></br> 
                <Row className="g-3">
					<Col className="d-flex align-self-stretch">
						<Card style={{ width: '24rem' }}>
							<Card.Img variant="top" src="https://www.nps.gov/npgallery/GetAsset/66F449BA-1399-4855-806B-D405505677FB/proxy/hires/100px180" />
							<Card.Body>
								<Card.Title>National Parks</Card.Title>
								<Card.Text>Find a beautiful park!</Card.Text>
							</Card.Body>
							<Card.Footer>
							<Link to="/parks">
								<LinkButton>Find a Park</LinkButton>
							</Link>
							</Card.Footer>
						</Card>
					</Col>
					<Col>
						<Card style={{ width: '24rem' }}>
							<Card.Img variant="top" src="https://www.nps.gov/common/uploads/grid_builder/trails/crop16_9/2D10594A-EBC6-A14A-5562C2B9EB292E27.jpg?width=465&quality=90&mode=crop/100px180" />
							<Card.Body>
								<Card.Title>Hiking Trails</Card.Title>
								<Card.Text>Find a hiking trail!</Card.Text>
							</Card.Body>
							<Card.Footer>
							<Link to="/trails">
								<LinkButton>Find a Trail</LinkButton>
							</Link>
							</Card.Footer>
						</Card>
					</Col>
					<Col>
						<Card style={{ width: '24rem' }}>
							<Card.Img variant="top" src="https://www.nps.gov/common/uploads/grid_builder/lake/crop16_9/8571C0A0-98B5-5F91-E0FEEB31FF48B678.jpg?width=465&quality=90&mode=crop/100px180" />
							<Card.Body>
								<Card.Title>Campgrounds</Card.Title>
								<Card.Text>Find a campground!</Card.Text>
							</Card.Body>
							<Card.Footer>
							<Link to="/campgrounds">
								<LinkButton>Find a Campground</LinkButton>
							</Link>

							</Card.Footer>
						</Card>
					</Col>
                </Row>
            </Container>		
		</div>
		</Stack>
		
		</>
	);
  }

export default HomePage;

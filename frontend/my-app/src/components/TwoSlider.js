import React, { useState } from "react";
import { Slider, TextField, Grid } from "@mui/material";

export default function TwoSlider(props) {
    const {min, max, discrete, onChange} = props;
    const [localMin, setLocalMin] = useState(min);
    const [localMax, setLocalMax] = useState(max);

    const handleUpdate = (event, values) => {
        setLocalMin(values[0]);
        setLocalMax(values[1]);
        onChange([localMin, localMax]);
    }

    const handleMinChange = (event) => {
        let val = event.target.value;
        if(val!="") {
            let numVal = Number(val)
            numVal = (numVal>localMax)?localMax:numVal;
            setLocalMin(numVal);
            onChange([localMin, localMax]);
        }
    }

    const handleMaxChange = (event) => {
        let val = event.target.value;
        if(val!="") {
            let numVal = Number(val)
            numVal = (numVal<localMin)?localMin:numVal;
            setLocalMax(numVal);
            onChange([localMin, localMax]);
        }
    }

    return (
        <Grid className="d-flex  gap-4">
          <TextField value={localMin} onChange={handleMinChange} size="small" />
          <Slider
            value={[localMin, localMax]}
            onChange={handleUpdate}
            marks={discrete}
            min={min}
            max={max}
          />
          <TextField value={localMax} onChange={handleMaxChange} size="small" />
        </Grid>
      );    
}
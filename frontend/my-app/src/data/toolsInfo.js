const toolInfo = [
    {
        name : "React",
        image : "https://cdn.worldvectorlogo.com/logos/react-1.svg",
        description : "JavaScript framework for front-end development",
        link : "https://reactjs.org/",
    },
    {
        name : "Postman",
        image : "https://user-images.githubusercontent.com/49151885/96122371-ad293c00-0ef1-11eb-9e1d-ef148ee03d73.png",
        description : "Tool for designing and testing APIs",
        link : "https://www.postman.com/",
    },
    {
        name : "GitLab",
        image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA4VBMVEX////iQyn8bSb8oybgPyniQSf8nQD8ayX9biT8aSX8piX8oiH8bCbiPiLhOx38ayjgLwDhNRP8pi76cTP8cSzhNxbgMAT8ojT8nxDwq6P87+3jSjL8ljf8nTb8kjf/+vP8jDbuYDb8eDDriHzlWUXodWb+9/bqgnX2zMf/8OD2bTb8rUT+2K/mUjT8hTX8qDb8slPmZ1X76OX2z8rzvbfunJLmYU7tk4n+7db8eBb9yY79z5z9uGT+3rryZTP8fTL+5Mj9jCr53NjkUDnvpZzys6z9vnP9xYX9tmL8sFDzdDdrIG35AAAKE0lEQVR4nO2ba1/TSBSHrWlS2tDS1rZEECsolYso6KoIiOiuuu73/0CbS5PM5cylzTkp5TfP++V0zJPzz5yZffTI4XA4HA6Hw+FwOBwOh8PhcDgcDofD4XA4HA7Hg2A2q6fMTi1lAA4nk0P6Nc4uJv1D8iogrwe+P74gL3Mx9v3BS/IyEH2/0Wj03xNXed+Pq/h94iogR4O4dGP8lLjM03FSZnBEXAbidS8p3X1FXOZVNynTe01cBmDWSySNNaVtdDv9tIrfq6dtsxxN0tKN8U/SMpmkjcakfk1f9rLSXdpuetHNyvTq76aTTFJiTeeSxppOCKuAfJhLGrc5Sk1/DvIykw+EZSBySYk1zSVdgaZ+LmnsD12b25mUZRpkVUBKSWNN/yIr89egLFOzpp97Zeku3XfxYbcs0/tMVgZi7Jel/T6VprN+gykzJqoC8oORlFBTVtJY0x9EZSBYSQk1ZSWtWdNdny3tj2k0nQ34MrskVUB+sO9Hg2xvw0safz3Vp+nHHl+aaG/zWizzkaQMBC8p2UdjXyxTm6aipESaHg3EMrVp+nEslib5aBQljfei/xCUgXjli6X9LkGZrlyGemQy570kKclH49FELkM+2cv4R5KURNOXkqS1aSpLStJNJ0AZ8sleynvAHgJNP8Bl6tD0KSApwUcjJGkNA+iET12oNHoaNwBJY00/IZcB2AE6aQJyGstfFfMy9CdtP0FJ0TX9DEpKPoBOuAAljTX1UcvIcT/XlPw4bwdscQmoW/AfqjL+hFrTn9LncKEp5t5G3J+VkA6gE1SSxv+6fyOW+VshKb2mM0WLS0DspqpOmpahPWcTBwssYzxN5f1ZCeEAOuFQKSmqpmpJSQfQj6TplwDaR6Oyk6b/kANKTXWSIu5toP1ZCammOkkR9zav9GUoNdW0uASkLTg0RODKoFQB0UuKtreB92clhJrK0y8eJE31klJerpmNdZ00AaWbwkMEBrpzNnlEK4KiqUlSwjtgJkmRtuDwEIGFTFNt3GcgbMFVQwQGf4CwGgCzpCh7G9UQgYXoDhg8/eJB2Nuo92clRJdrVIMFlupb8B1oEiyVoTgnUYxoRSqnsXqIwEJyucZGUgRNbSQl0tRG0kblLbhuiMBAoamdpJU1NX365hBoqhrRilTc2+j3ZyUEl2uaftOOamk8sKziN5HWVXD5pfXYjo19f3n2NyyrtL5cIq/wKhxtWhY/qbDCE8sam6PwCnmFw3B7y7L6hnjdxhp/17LE463tcIi7wOPA6zxrW65wf8kFNhq2krafdbzgGHWFV5Hndd5aLvGkueQCm5aStl90PC/C1fRd6Hne8MCu24x2l1zh7sjq77cOhvGvCd9hLjCWNGG4ZdVtltbUTtLNrWH6a1A1vY7Sv+nt2Xm6pKaWkrb3sh8TXSOu8DTM/mjnudUSR0t1U99O0vbzTvZjwlO8Bc4l9Wy7zZKaWknaftvJfwyiprmkyatoFfxLaWol6eZoWPwWRE1zSROsgn+01DO0kXRru/wpeJpOg/Kv2gX/UpraSJpEfUkwRVrhm8hjl/jCYolLaGojaRr1JdEbpBWeh+yftQr+JULfopO2vg25XxKe4yyQkzRd4mNjt9k4W3iFZ0ZJ2S6DqikvacKeudvcLpqI/q3xb27tiT8ESVNBUs8q+EeLvohNo6RF1GNrKknq2QT/wpoaJWWiHlnTr5KknkXwbyyoqX9rWOHmgfgSppp+RVjhjSRpgjH4nyymafOJ4e+xUc9oelN9gZCkCabgX1BTk6TtZ/DPQNAUlNQzB/9impokFaIeVVNY0mSJ3/TBv5CmBklb3xQLRNB0qniEnrHbbJzZP0RfL6kc9cxDrLrCO8VrmKDf8S+iqUFSOepLgruKK/ylktQzBv8CmuolBaK+JPxVcYVqSZMlvtW9ivaa6iVtQVFfUlFTnaRess3QvIr2mmolhaO+pKKm19pnaAh+a021ksJRzzzDarOMK8MKtcFvq6lW0ramy2QrrDb7Nj1DbfBv7FuuUDO/UEY91jM81r+Hnj74LTXVSKqO+oKqM8Ub00PUBr+Vpv6Z8r/XRX3+CKt+1Ew9TSBmqIPfTlONpKYuEy9wWPnTe3puElUT/FaaqiXVRn1K8C/GFvg6MDxGTfBbaKqW1BD18fdMgDT1vgwNL6My+G0mw0pJW4aojw3FO7j4ZTBVGfwWk2GVpJumlzCo+kXK8cZgqqrbmM/ZlGdqhqgPA4wRDcPxqf4bXBH8FpoqJDVEffQH68yi5Lf2MaqC36ip4rhCH/VhgH2bJuUu0i1REfwmTRVXaPRRH4V3FAs0ReM26KlRU1hSbZcJbvANzdFFo+Jg0aApLCl/TMgTBlhnaiDHQ3XDgYPfcM4GdlJd1EfvcO9CyWiiETxYNGgKSaqLetwQhPmqbjjgjSKtppCkm4+VL2GIck5hRBONUPBrNYUkVUd9dErXYniuVA0H2mZoNQUkVW4oiEIQRhmNnW/yEjWaApK2VVEfhdi3grVM/1U0HCD4NZrKkiqjnjIEYVTRKG8zNJrKkiqinjgEYRTRCAS/UlNZUsUxIX0IwsDRKG8zlHfAJEkVG4rg90rWF/MVNLUjBr/yOFicBLcOoAWi7wQXAY5GsduoDjDE4wq4ywTndbcYHnDXKAa/YuQmXqGBor7WEISBolEMfoWmgqRQ1EderSEIA+0aheCHNRUkhW4E1R+CMEA0Cq/iE1BSbsgGHBOuJARhLuVo5IMf1JSXVI76VYUgjByNXPBDmvKSylG/uhCEkaKRD36gm3KSSlEfRnerXpKIFI38fFE6wOCOK6TZ4apDEEbcNbLdRtaUlVSMerQTF2zEaGSDX9KUlVSI+pp3gosw43eNbPCLtxbY2wlC1NcxbFoePhqZ+aKoKSMpPzu8RyEIc/yObTjMwaKgaSkpH/XR6X0KQRguGpng5zRlOin3//nctxCE4aKx6Db8rYXy4JftMvcwBGGmf0pTy+DnNC0kZaMe59pBPTDRWAQ/202LTspE/b0NQZg7r3iMefCzI7dcUibqMa8d1AIzUM0PFhlNc0nbRZe53yEIU1xuyIN/VGjqn2Xzi+KYcKXDpuUponEe/IymmaRF1K9DCML8DrjgLzTNJsF51K9JCMLkZ43b2cHi/NaCv5s8wvxG0NqEIMz0T1AGf6FpKuk86u/nTnARsm/xLPjnBxippFnUr1kIwlym0ZgG//ycLTmuyKI+Gt7bneBC3ATz4J9rGkuaRf06hiBMGo1J8J/4c0mTqF/TEIRJxlTJwWJyByy5jJhE/fqGIMzvIIyDP9V0fyOO+rUOQZi7KBwetOJu2jxpHQxDqrt3q2R6HmxvxZrujra21z8EYa6Dvfa+v9/eewghCHM5/H7bvP3+QEIQ5teX3S8PJgRh3vz3gELQ4XA4HA6Hw+FwOBwOh8PhcDgcDofD4XA4HA6HQ+B/OlwEKa3k0bcAAAAASUVORK5CYII=",
        description : "Git repository and CI/CD platform",
        link : "https://about.gitlab.com/",
    },
    {
        name : "Bootstrap",
        image : "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/1280px-Bootstrap_logo.svg.png",
        description : "Styling React Components",
        link : "https://react-bootstrap.github.io/",
    },
    {
        name : "NameCheap",
        image : "https://static-00.iconduck.com/assets.00/namecheap-icon-512x512-jcdkwntk.png",
        description : "For getting our domain name",
        link : "https://www.namecheap.com/",
    },
    {
        name : "Flask",
        image : "https://miro.medium.com/max/800/0*3ym_K6CR2DY2JkfV.png",
        description : "A webframework for building web applications",
        link : "https://flask.palletsprojects.com/en/2.2.x/",
    },
    {
        name : "MySQL",
        image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA3lBMVEVEeaH////miS5Bd6DoiSw6c51de5Q+dZ82cZzPhz75+/zSh0JUepTpiSrB0d4/eaPR3efK2OPt8vYweKcrbJnqiiNMgKbr8PRJfaSkus3i6e9mkLFYiKuqwNJCeZ90mriPrMRtlbSsg2O3y9uJqcKZtMpejK/a5Oy5hFR6nrpLeptkfIzHhkbZiDfYiDneiDPBhUyLf3qYgHSzg1mJf3d8foOggXLFhVB1fn5yfYWxg1+Df3iWgWvChUakgl9rfI1rfI+/hFOVgW+mgWt7fYWcgXKcgmWlg12PgW98fnr+POTxAAAUnUlEQVR4nOWdCZuaSBOAwbYHQpRWUFQExcEjOndmZpNJstkj2dnk//+hry8OlVMQyLf15BRjeK3qrqOrG0E8ryiaNjL7rr1azqdryzLGwtiwrPV0vlzZbt8caZpy5jsQzvfRitlTbWe+NmRZhlACQBAE/yeQIMSvG+u5Y6s984yY5yI0XWexNgCEFCxeAHmLsV44rnmmOymfUNFMe44GqWyHnAM0t80z2GzZhBt1tQBydrY9ThksVuqm5DsqlVBx55aQR3cRuhSsuVuqIksjVEbqVJZBATwPEsjyVB2VRlkSodJ3LFgULoQJLadfEmMphIq9MErkY4zGwi6FsTihsnFgyXgeJHQ2xSGLEmLzRGfhY4youLEWJNzMx9KZ8JhI43lB91GEUOktTvR8eQTIi14RPRYg7DlnM88DRuj0aiBUloZUDSBGlIzlyWo8kVCzEawIjwlEtlYlYX9aLR9lnPYrI1ScM0+g0SKNnVNMNT+hosLqFcgEQjU/Y25Cc1nV/BIlYJk7Uc5L2LMqm0EjCSUrr+PIR6itKnKBCYxwlW9SzUVoLmoHJIiLXJaah7C/rmuK2Re4zuM3shMqqwqC0GwC5FX2OTUz4Wgp1w0WEnk5KptwtGiKApmARVbEjIQbWEcUkyQSzJg3ZiNUjWZpkAgw1PII3XHzADHi2C2L0B7UDRMjA7scwonQRA0SAcKkDMJV3RyJsipOuBo0VYNEwCAVMY2wuSbKJN1QUwjtugkySMp0k0zoNtpEmYBBstNIJFTHdd9+Jhknuv4kwk0DI5koAUZSAJdAOGpAuptNAEwIw+MJR4umBdvxIiVkGrGESq01tbwC4qv+sYSrJiW86SLHev44wv6vBYgR42o3MYTm+leyUSJgHVOBiybUFtVV1YAESykyw0V0HTWacFUdoDx1VlurjDIejB6KkYS96jyhvNIUURn113JhRQIYWfCPIjStygAlRxE1al2bbfF2KitqKEYQKk6Frl4V1fV0Qr981yj6/0pRC4wRhGrhLzO7jPviFgJo0JaSnjUYDOQCnX8ARMTgx4RKlYsTwBVV8v9J8pZaGB6ReN45nREeK/GY0KmSUNoqypwaJzT8eWLjzk+NN6CTTtivNCcEoC/2WJIGjK0zsVkztDKRTpzOx0ehzSGhNq04o5A1UeUao83tA2NC8oSRc5qpStNDv39IaFe9RAi3yv7AALKxIvOOa530XcPDss0BoYJKuvHsAmxx5BkO0xuQLNJaai5OGo1ISSRc1rDKC1Vv8BvzNXsJyLQhwTllvQQukwh7Rol3nlWAoYhLokSwFkdbT5sC6Wd3T6kUGb14wkqjmUDkCU9H4UQceZYJBJK2904oZx5ENnuEFUbcYQF4/mPGM+4r/joXgAKeVDf5Y+SDCHyPsK6VbKPP8zXJmYQmAmm6IaFcfsRFHOGmrsqFZIsbSgb2ww2at/fyr17Km2hCZV5X5QIsNC3SfgDEiGr+z5srkYTVxmt7Im3ESaSfAlM8Fie5v/lw7BYQ1jSRUsEBcz/aUZFCqDLP66XD02lAuKk+nAlkoIjbaE2RQqiS2/OjTQRhpVnToeBosh8zo8jqCUl5KIvyCZVa12HwXCPG9I6ToajkjSZBkAr7hHa9RW4JhzMxVWiIB1XuCrVsHxIqNfetkTXAWOfey5/VgYVyQBgzlVUnwFLiCtFgim8w7yAy+vuESq3zDJWBG+MTscm5Yuy1OIGew+CEo+qKwHEiY4cR45FJjGLmjEeANdojVGtXIU4O46cTbHK5p1Oo7hFOa1chzg3d2JsgJpc3qKSj1ydU6l8PlXvKMj5sNMz8iYGshAjd+gmNkZZgSFjDuW9SdkOEteVNvoCtYiatrxGF5P3IeUB4QqmgbMHZQC9JRyRGyen1gbXxCdX6GxDhymuOiL4VErjmdGlAUD1CpcJF7TjBA40X9+dW5BskPKrsfIqAdN8JIYyuIFQrHiHWVXQmDuZabGweI4D2LhBCs8Il0TjBVkrH4WATlyjCft4OCgBMTlhz4kRFWiom5CX56EI3DnpEJV+hhaZQQiN8BTXCEU5vaDE3xvHJvbzxN/UXhLDOAo0v2OPj6YCGyzHDjcTf2naQJ41CjNBsxIYRrCEbgq0mqr24AFUiTesjJ8cy/8CkhG79vkIgA1HsGSTCdtdxkzuw6PlKpm1kbaGCLiWsP/klAiRR3A6wliZ4Oo0ZiWA8n5Dp0cy6HZmU3ITaKzSeYH8xMlySB+LxFtNNAAAcWDaG3GTzjKRaIzSm0ZKEkaYpkgzDFnsJ75PHK/y2TJuVyNKOUM+6b5SwEKQHsTH2kn27tMYRZz/L4qnRw4TxmXXVIo37pkrNc7DREp00GCw10cwwvICLCSvvL0kQWeL5hdVLWcyUpmqWzdfQFoUG1BEjBBj9SfLtQwFnDqmLUtj9CMnmUJuAwWobfxFKY8MytooSs2AVvHOuCVpDptJDAeNt9JYWIMnTldvvbcyNkppPgbUmjJoylR7JeB41lQBh2fPOAFVUJ63hxhgJZgNSpxgBRkSJFGxFekaqu7XQIL05XDaFJm8dibx90vLUm4KMWxjkntCAUmk+oSv7TtZeKdkVmuQOswnd4uRmPOsI2kID6mx5Raa9mdnyC7gSEhYLmipA2Gp0UTeDcqSl0EyHnywAyuTQz9E2HRHMhfrX1U4SsMXphZaOCKbCuor7OYNI4+1GHKXvsVsL0TX0sADI5FjZEruQeU8WQrNdp7PbzVBEeY9c3HUuIq4iKke3JQ2mo3REK50QbCdU7KMQUFqyK8uMTVn61cPlu/vr+3eX719mRxQXT+Qivvrp4Wrv6u7hPZaH3fEHSoabWqGwhNROauAdWXAUGwx4eLhJtwMseufyttVl0r65/rxHoXc+vSUX2238S+v2vqP7l9DFG/Ly207UveEgNXnxGxhC2uo48E8sOIzR+Qpkxi216OFu2G550m4Pb376iGj3ZYgpgqvd4WPHu4oJ8ZV2JCEeQesUZ5Bh9d9vfVcOmkFkDz1pedqH+Mn42kyH+E/Df3097R67/kXOObzfccREQm+LRpKk6tDyGxntfULD38+Y7lPRyw259faw9fu/r1/f3QyH3dYfnpb0e6q/7vDm/vH+Zsggh4984KUQpsk4dRzSLl1vvIXfC4Mu1bhOn0B2910CePcV6Tr+oc8+fvg249fQ65Da7eOFTqTztUURu99QCYR4HKbNEkEL3EG/BwgOTUmNbdF3BhgaesibG9HLHSG4+8IdAtKfmL5vmZIL6jDdW9CMk0t4bStkvenlOv0rIew+Rq1y7f6i1y6DV9APqsUuG6iFCdNiGtZty86310L+QqLkbBZS03JM/Q2dW16iHP13aqPXs/Br77p0KJahw3VqXArpOUwqQwkNRLKsI2psCzxdnwYykb19duRZOTIZo/odJbyKINQpzfApfAldDSkhNeqC43CamluwziKX9U2HapgDmsCw3rHRgHyUq2KxA6cJFvQVssCgU7MbfowgnDGY2d6LOh2J3Xd6ccJ5an7IvJ69pmZqhm6fEiM2EMkaq8Hm3KCCybRPBymz0u5fs6OPR58JYfde33uVjdv2211hQpwfps2DMrXPiUB/0/z7p6rV5gNGKAt+G1kw60j0BdqPx0yxffd0pETGMvx7/wJ6poPzlph1MUKc46fNgwPq11e8M9x/95iuVHqhKQkbeBDnr5nzliS6noseqC227z7qBx+PPlDC5wPCP1vUgzwXJ7RTa22MYQnndAextxQn0TN9JrLM4hoamHJafyCyc6mpYaPOGzo7toevnX2Wzlvq3v88IKRUrdb7woSyKyT2y5H79qIWto2cl6GZu7cAs2EWtvE41TN7sKYDk9djn1ig0upeP4VTPfRyS27/9uLAfDu/UydZAmE/teYNOAJ0w/dP9YPdI6ei05W0DFyH4AdDHvBXnll0W39d6UFoQ+PV9ptDQhbldX8rTmimrVsAi6uO/YEPMxbo2LLAuWmww0/58TINyQkMGIv+5a7rpUbvLrzkED3fRRMyL/mtMKExSlt7Yl6BGOeAbeWnkTqbd/DEyne/08HMpxav05HPrYGJPN97CWL35hsfjugzJbw+vH2cT9HgWyhISNaeUtYPWXBG9BI2U0pLjrHh5/rwjcrslG3erco2coaWctHuby8Jbrdvf7Kg84H+NYawuA7J+mHKGjAbXCNMyENw0qtMmuiw4iTfFBkH3+LAU345sF9P9N3rHU9x28NPu4Dw2EofyxmHZA04JTFgSiI37Q2zBbdNugFC4thMU3wTNU0X+bg9SB31l8eWZ6rvkG+lMeOwXXguJev4Kb0YLPQiuS8fZkQriLDSnmTPzTPD5Lv+aBsZO9rgaI0W7V5uuamSQIbPNEfegqVUrafCVqqm9tOw0UeraTyp7xvksA7Ra+llhPwzIP0b3b3BTohTjwuNCH25Y1WZmz+QV93489AfXre9jLkQIe2nSa6UMYdHvwU+zLQpm0CZLTKn5/WY8wCAfCILxCP7QdFPXnj6DTGP3xp+P4xpWCBQNC5lPVHJfW3snpme+f07Y/o7rYEDdhyMZ4x8asVJlkSXh7ToXgn0ZchG4g6xqG34dBSXlpJbsL625N5EFniy3Yl8WtnQpJDl9bxO5bVLcmDTc5SbmIL/jgVl1x3EIu/D7Ak90Zzqr6L5IetNTO4vlRghqzmyCE6kN8++F2+C9XTF9jVqC4ntiI+bp2eXXe4k9E/0T62D/JCFND+K5vi8vzSpRxiwgJtPGPw0BkX0YzNej/I3ZTHVKSuZhd1xQxz9xoozFzwDPixwIFYToOW4IoS8Rzixz5uVfdU9bxCeJDmhZ+msv1BU2Ttjvzumw+49RkBdlv7vrWLQccqKGIUIeZ93YsmaTYnq3jALT5Ks5u/vugGAEveEfuxM6t90q3s5C9L/UA6MLuj0030uWmvze/UT9ltwD+FFBf4aht+JzV/xayEsJh95GYn/QTM9pCT9lQ2+B+QVLAiBvxQze2Rq3YW+jtD17OLvt0jYM8PTWI8Qeke8bzwivmXa1xbPtuywJeO7/KN1/0eHL3WizitzFh9oYWpH55pW9+5lR6/OLq6DIk2YUJh5wr4z+kM4Lm4FN+/vmUnY98Tt0vVOb5pzM/V1xk8RCSZNmnYoI3Fvfzg2xW77928PL1dXV8/frnlFgyGgq1vm/1uP759fXh4ub1k52HORnPDu06Uv7xG6urx8vXj6R5h9+h6r22DfU8LeNT5zeA6FT62hjTt8dTEglP3ljNDu6w4JRbvd1s3t21ueXbT9+hqeTvnaWvvu5qbFLne/7q8f8mU5KsMPurD7effn7MObq4/Df2IJg71rCfsPudJ8lykzMw0Kapww8KmSf5hYOJJ4vSU33qbCaG6DMEb/eMPS/5Z3udW+v0J7OgxLFxOizo2uf3j9dv9vLGFo/2HCkayABl+BinhOFAw7nnAECYq/VqWFWxvQ1bebYde7xXYXD8u9etTjsLuP0b39Z28cHhFeEMKP1/c/foslZAe2puwDlmgaETZCbf/9/MRQNXjBO5N53/TxBPLj9yGX9uP33b6H3z0/3pErXXqd6rt7+3XGorbb4YFcE8I7Xb++enz6Ek8Y2gecsE1aGhAJ3Sz9e+gLgfSF4N97R90eJWUI6ejq49OPzx87un50V0jXOx8/f/ny4+eFfvGOFq3wuPy6Y5cOhfwL/IuOPzKqcYXJ3l7uEvfjcyuN3lwf3RmzdxFfRrOf1yzSOcqM88jefvwSz1Tg5f9+oX2pqPMFa7DV/rsA4P6ZCuWdi8EzLK2gUSB99+5u+CHBnafKwbkYxc82ARIWKLAHTGrpvQupgp7vo9aMM9/PwdkmhfeVgPFyuV3aJrP9VRlGjyL6vLLL4fk0xc8YCj0JRdk2oHf88Iyh4rsQg1NDzSY8C/L4nKjCG7x8wuMexjoEHp31Vfi8NiAt3M1GdYRGPK006ry24tuBaXNJQzr/o87cq/fcxLIl8tzEOs++LFuiz76s8/zSsiX6/NIaz6AtW+LOoK3vHOGyJe4c4frOgi5Z4s+Crus875Il6Tzv/4/pNOlM9uacr1BEks7Vr+fZCCVL8rMR6ni+RdmS8nyLRp0hcZKkPaOk+ufMlCzpz5n51WO39GcFNeVQpRMly/Oeqn1mV9mS6ZldlT53rVzJ+Ny1mIL8ryBS1BNla37+YamS+fmHv2oEnuMZlpU+h7Q8yfMc0kqfJVuW5HuWbGOOqcsuOZ8H/B94pvN/4Lnc/4Fnq4uj6MPEGinSYhTLEU8ojn4ZrwhgPGASobg55emDNQgwNgkUSYSi+mvkiuOIeDsjoeie8IDFqgUM3ESGZELRrvv+M8hhYSYfoTip/7EJiQKESQpBGqG4arShgkGsp89MKGY6krg2SQXMQNhgQ0030WyEot2IY+kjZJAyyWQmFN1Tnq18dgHjZDeRh1BUGxjdACPR0eckFDewaWG4BJNCtfyEONNolhZBQjZxGmGW48ErFHmZFTA7Idly1xQ1AnkVm/AWIBTF/roZFTi4jqvJFCUUzUUDkmIAFzFVtRIIRS3rw0HOCbiKrouWQ0ge5p75tNKz8ElWZOm+RELRdGolXOay0JMIRUWFdU04EKrZ59DTCUnr1LiOCEcaO/n5TiMkJ9FVr0Y4zeMjihKKmo2qZYTIzjeFFiUkVX+jslkVSEZ81f5shNhxOBU5RwCdvC6iHEJR6S0qCFWBvOidrMCChFg28zNPq9J4njEPPBOhqPQddDZjBRA5/SL6K4MQM24ceBZjBTJ0NkX5yiAUySERC6NkRQJoLOzieGJJhNRYrRIZgWwVN08uJRGSozDUqSyDwj1x+BNkeaqOSuIrkZCI4s4tARaABAAK1twtjY5IqYRYNupqAU6cePC/W6zUgs7hSMompM/TsudoAPPoEusODtDcNrVS1UelfEImpuss1gZI5SRswFgvHDd3aptRzkWIRTF7ru3M1wbZWgoliur/BBIkW06N9dyx3Z5Zvup8OSMhFUXTRmbftVfL+XRtWcZYGBuWtZ7Olyvb7Zsj7Qx2uS//A2IHkJoimCNjAAAAAElFTkSuQmCC",
        description : "For database management",
        link : "https://www.mysql.com/",
    },
    {
        name : "AWS Amplify",
        image : "https://miro.medium.com/v2/resize:fit:1200/0*y-ZimaRh8fnftZVZ",
        description : "For web application building and hosting",
        link : "https://aws.amazon.com/amplify/",
    }   
];

const apiInfo = [
    {
        name : "National Park Data",
        image : "https://upload.wikimedia.org/wikipedia/commons/1/1d/US-NationalParkService-Logo.svg",
        description : "We called the API to collect information on all national parks. We stored the result of this API call in a JSON file and then created a python script that parsed the JSON file and added each national park with all of the desired fields to the appropriate table in our mySQL database.",
        link : "https://www.nps.gov/subjects/developer/api-documentation.htm#/parks/getPark",
    },
    {
        name : "Trails Data",
        image : "https://upload.wikimedia.org/wikipedia/commons/1/1d/US-NationalParkService-Logo.svg",
        description : "We called the API to collect information on all trails. We stored the result of this API call in a JSON file and then created a python script that parsed the JSON file and added each trail with all of the desired fields to the appropriate table in our mySQL database.",
        link : "https://www.nps.gov/subjects/developer/api-documentation.htm#/thingstodo/getThingstodo",
    }, 
    {
        name : "Campgrounds Data",
        image : "https://upload.wikimedia.org/wikipedia/commons/1/1d/US-NationalParkService-Logo.svg",
        description : "We called the API to collect information on all campgrounds. We stored the result of this API call in a JSON file and then created a python script that parsed the JSON file and added each campground with all of the desired fields to the appropriate table in our mySQL database.",
        link : "https://www.nps.gov/subjects/developer/api-documentation.htm#/campgrounds/getCampgrounds",
    },
    {
        name : "Google Maps API",
        image : "https://solamaragency.com/wp-content/uploads/2017/09/google-maps.jpg",
        description : "Google Maps API is a geographic interface. We use it to show users where locations of parks, trails, and campgrounds are on a map.",
        link : "https://developers.google.com/maps",
    }
];

export {toolInfo, apiInfo}
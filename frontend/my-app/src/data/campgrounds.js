const data = [
    {
        "id": "58B9591C-4723-4049-B26C-D30C07A6FAD6",
        "url": "https://www.nps.gov/yose/planyourvisit/bvcamp.htm",
        "name": "Bridalveil Creek Campground",
        "parkCode": "yose",
        "description": "The Bridalveil Creek Campground is located along the Glacier Point Road near Bridalveil Creek and is surrounded by a beautiful forest of red fir and lodgepole pine. At an elevation of 7,200 feet (2,200m) the campground is located 7 miles west of Glacier Point, 9 miles east of the Wawona Road turnoff, and is approximately 45 minutes from Yosemite Valley. The spectacular views from Glacier Point are nearby, and numerous hiking trails are located along the Glacier Point Road. There are no services nearby.",
        "latitude": "37.6636576342436",
        "longitude": "-119.624293366359",
        "latLong": "{lat:37.6636576342436, lng:-119.624293366359}",
        "audioDescription": "",
        "isPassportStampLocation": "0",
        "passportStampLocationDescription": "",
        "passportStampImages": [],
        "geometryPoiId": "0C572B6A-D624-4AFC-B0CA-9467F80775AE",
        "reservationInfo": "<b>The Bridalveil Creek Campground will be closed in 2022.</b> This campground is typically open on a first-come, first-served basis. Reservations are required for the horse sites (3) and group sites (2). Campground reservations are available in blocks of one month at a time, up to five months in advance, on the 15th of each month at 7 am Pacific time. Be aware that nearly all campground reservations in Yosemite for the months of May through September and for some other weekends are filled the first day they become available, usually within seconds or minutes after 7 am Pacific time.",
        "reservationUrl": "https://www.recreation.gov/camping/campgrounds/232453",
        "regulationsurl": "https://www.nps.gov/yose/planyourvisit/campregs.htm",
        "regulationsOverview": "<b>Pets:</b> Permitted (except in group sites); must be on a leash at all times. <b>Fires:</b> Wood and charcoal fires are permitted. <b>People per site:</b> A maximum of six people (including children) are allowed per campsite, except for the group site, which requires 13 to 30 people. There is no limit on the number of tents (as long as they all fit into the campsite). <b>Vehicles per site:</b> A maximum of two motor vehicles are allowed per campsite. All vehicles must be parked on a parking pad. Trailers don't count against the vehicle limit as long as they fit on the parking pad. Additional parking is available near each campground for no additional charge. <b>You are required to store food properly in order to protect Yosemite's bears.</b> Learn more about <a href=\"https://www.nps.gov/yose/learn/nature/bears.htm\">bears</a>, <a href=\"https://www.nps.gov/yose/planyourvisit/bears.htm\">proper food storage</a>, and <a href=\"https://www.nps.gov/yose/planyourvisit/lockers.htm\">food lockers</a>.",
        "amenities": {
            "trashRecyclingCollection": "Yes - seasonal",
            "toilets": [
                "Flush Toilets - seasonal"
            ],
            "internetConnectivity": "No",
            "showers": [
                "None"
            ],
            "cellPhoneReception": "No",
            "laundry": "No",
            "amphitheater": "No",
            "dumpStation": "No",
            "campStore": "No",
            "staffOrVolunteerHostOnsite": "Yes - seasonal",
            "potableWater": [
                "Yes - seasonal"
            ],
            "iceAvailableForSale": "No",
            "firewoodForSale": "No",
            "foodStorageLockers": "Yes - seasonal"
        },
        "contacts": {
            "phoneNumbers": [
                {
                    "phoneNumber": "209/375-9535",
                    "description": "",
                    "extension": "",
                    "type": "Voice"
                }
            ],
            "emailAddresses": [
                {
                    "description": "",
                    "emailAddress": "yose_web_manager@nps.gov"
                }
            ]
        },
        "fees": [
            {
                "cost": "36.00",
                "description": "Bridalveil Creek Campground Reservation Fee - Non-Group Site/night",
                "title": "Bridalveil Creek Campground Reservation Fee - Non-Group Site"
            },
            {
                "cost": "75.00",
                "description": "Bridalveil Creek Campground Group Site Fee - Group Site/night",
                "title": "Bridalveil Creek Campground Group Site Fee - Group Site"
            },
            {
                "cost": "50.00",
                "description": "Bridalveil Creek Campground Stock Site Fee - Stock Site/night",
                "title": "Bridalveil Creek Campground Stock Site Fee - Stock Site"
            }
        ],
        "directionsOverview": "The Bridalveil Creek Campground is located along the Glacier Point Road. From Yosemite Valley, drive south 17 miles on the Wawona Road to the Glacier Point Road turnoff. Follow the Glacier Point Road for approximately 9 miles and the campground will be on your right. From the South Entrance follow the Wawona Road approximately 16 miles north along the Wawona Road to the Glacier Point Road turnoff. Follow the Glacier Point Road for approximately 9 miles and the campground will be on your right.",
        "directionsUrl": "https://www.nps.gov/yose/planyourvisit/driving.htm",
        "operatingHours": [
            {
                "exceptions": [],
                "description": "<b>The Bridalveil Creek Campground will be closed in 2022.</b> The Bridalveil Creek Campground is typically open on a first-come, first-served basis from mid-July through early September (exact dates vary).",
                "standardHours": {
                    "wednesday": "Closed",
                    "monday": "Closed",
                    "thursday": "Closed",
                    "sunday": "Closed",
                    "tuesday": "Closed",
                    "friday": "Closed",
                    "saturday": "Closed"
                },
                "name": "Bridalveil Creek Campground"
            }
        ],
        "addresses": [],
        "images": [
            {
                "credit": "NPS Photo",
                "crops": [],
                "title": "Bridalveil Campground",
                "altText": "A wood sign at the entrance to a campground reads, Bridalveil Campground.",
                "caption": "The entrance to Bridalveil Campground",
                "url": "https://www.nps.gov/common/uploads/structured_data/F2FB2779-A1A5-985F-2488256AA422403C.jpg"
            }
        ],
        "weatherOverview": "Yosemite National Park covers nearly 1,200 square miles (3,100 square km) in the Sierra Nevada, with elevations ranging from about 2,000 feet (600 m) to 13,000 ft (4,000 m). Yosemite receives 95% of its precipitation between October and May (and over 75% between November and March). Most of Yosemite is blanketed in snow from about November through May. (The Valley can be rainy or snowy in any given winter storm.) Check the <a href=\"https://www.nps.gov/yose/planyourvisit/weathermap.htm\">weather forecast</a>.",
        "numberOfSitesReservable": "5",
        "numberOfSitesFirstComeFirstServe": "110",
        "campsites": {
            "totalSites": "115",
            "group": "2",
            "horse": "3",
            "tentOnly": "41",
            "electricalHookups": "0",
            "rvOnly": "0",
            "walkBoatTo": "0",
            "other": "69"
        },
        "accessibility": {
            "wheelchairAccess": "There are no designated accessible campsites at this campground.",
            "internetInfo": "",
            "cellPhoneInfo": "",
            "fireStovePolicy": "Wood and charcoal fires are permitted.",
            "rvAllowed": "1",
            "rvInfo": "Bridalveil Creek: 110 sites with space for tents, RVs up to 35 feet, and trailers up to 24 feet. Not every site can accommodate equipment of these lengths. Be sure to read the site details when making a reservation on recreation.gov to ensure your equipment will fit into the site you're reserving. You can also find answers to <a href=\"https://www.nps.gov/yose/planyourvisit/campingfaq.htm#rvlength\">frequently asked questions about length limits and site types</a>. When making online reservations, you can filter available sites by entering the length of your RV.",
            "rvMaxLength": "35",
            "additionalInfo": "Each campsite contains a <b>fire ring</b>, <b>picnic table</b>, and a <b>food locker</b> and is near a <b>bathroom with drinking water and flushing toilets</b>. In Bridalveil Creek Campground food lockers measure 33\"(D)x45\"(W)x18\"(H). The group site has five food lockers. <b>Groceries:</b> Yosemite Valley (Yosemite Village Store; Curry Village Store; Yosemite Valley Lodge Store) and small grocery store in Wawona <b>Showers:</b> Yosemite Valley (Curry Village) <b>Dump Station</b>: Available summer only (on Forest Drive east of the Wawona Store) and all year in Yosemite Valley (in Upper Pines Campground)",
            "trailerMaxLength": "24",
            "adaInfo": "There are no designated accessible campsites at this campground.",
            "trailerAllowed": "1",
            "accessRoads": [
                "Paved Roads - All vehicles OK"
            ],
            "classifications": [
                "Developed Campground"
            ]
        },
        "multimedia": [],
        "lastIndexedDate": ""
    },
    {
        "id": "1A5B19B8-BF1F-4803-8D87-2D4091D59A4D",
        "url": "https://www.nps.gov/yell/planyourvisit/bridgebaycg.htm",
        "name": "Bridge Bay Campground",
        "parkCode": "yell",
        "description": "Bridge Bay Campground—elevation 7,800 feet (2377 m)—is located near Yellowstone Lake, one of the largest, high-elevation, fresh-water lakes in North America. Campers at Bridge Bay will enjoy spectacular views of the lake and the Absaroka Range rising above the lake's eastern shore. Yellowstone National Park Lodges provides reservations for this campground.",
        "latitude": "44.534437749236",
        "longitude": "-110.436923682403",
        "latLong": "{lat:44.534437749236, lng:-110.436923682403}",
        "audioDescription": "",
        "isPassportStampLocation": "0",
        "passportStampLocationDescription": "",
        "passportStampImages": [],
        "geometryPoiId": "451A1473-7B3A-448D-8571-B69016C2B5C0",
        "reservationInfo": "Yellowstone National Park Lodges provides reservations for in this campground. Call 307-344-7311 (307-344-5395 for TDD service) or select the \"Reservations >\" button to open their online reservation system.",
        "reservationUrl": "https://secure.yellowstonenationalparklodges.com/booking/lodging",
        "regulationsurl": "https://www.nps.gov/yell/planyourvisit/rules.htm",
        "regulationsOverview": "Overnight camping or parking is only allowed in designated campgrounds or campsites. Campsite occupancy is limited to six people per site. Checkout time is 11 am. Camping is limited to 14 days from July 1 through Labor Day, and 30 days for the rest of the year. There is no stay limit at the Fishing Bridge RV Park. Unless posted otherwise, wood and charcoal fires are permitted in all campgrounds except the Fishing Bridge RV Park. Propane grills and stoves are usually unaffected by fire restrictions. All odorous items that may attract bears, including food, cooking gear, toiletries, and garbage, must be kept secured when not in use. Bear-proof storage boxes are available at many campsites. Where permitted, generators may only be operated from 8 am to 8 pm (60 dB limit). Generators are not permitted at Indian Creek, Lewis Lake, Pebble Creek, Slough Creek, or Tower Fall.",
        "amenities": {
            "trashRecyclingCollection": "Yes - seasonal",
            "toilets": [
                "Flush Toilets - seasonal"
            ],
            "internetConnectivity": "No",
            "showers": [
                "None"
            ],
            "cellPhoneReception": "Yes - seasonal",
            "laundry": "No",
            "amphitheater": "Yes - seasonal",
            "dumpStation": "Yes - seasonal",
            "campStore": "Yes - seasonal",
            "staffOrVolunteerHostOnsite": "Yes - seasonal",
            "potableWater": [
                "Yes - seasonal"
            ],
            "iceAvailableForSale": "Yes - seasonal",
            "firewoodForSale": "Yes - seasonal",
            "foodStorageLockers": "Yes - seasonal"
        },
        "contacts": {
            "phoneNumbers": [
                {
                    "phoneNumber": "307-344-7311",
                    "description": "",
                    "extension": "",
                    "type": "Voice"
                },
                {
                    "phoneNumber": "307-344-7456",
                    "description": "",
                    "extension": "",
                    "type": "Fax"
                },
                {
                    "phoneNumber": "307-344-5395",
                    "description": "",
                    "extension": "",
                    "type": "TTY"
                }
            ],
            "emailAddresses": [
                {
                    "description": "",
                    "emailAddress": "reserve-ynp@xanterra.com"
                }
            ]
        },
        "fees": [
            {
                "cost": "33.00",
                "description": "Rates do not include taxes or utility fees and are subject to change. Interagency Access and Senior Pass holders receive a 50% discount.",
                "title": "Nightly Fee"
            },
            {
                "cost": "10.00",
                "description": "This is the per-person cost for a hiker/bicyclist campsite for one night.",
                "title": "Hiker/bicyclist Nightly Fee"
            },
            {
                "cost": "165.00",
                "description": "Nightly cost for a group campsite with 1–19 people in the party. All rates subject to applicable taxes and fees.",
                "title": "Group (1–19 People)"
            },
            {
                "cost": "250.00",
                "description": "Nightly cost for a group campsite with 20–29 people in the party. All rates subject to applicable taxes and fees.",
                "title": "Group (20–29 People)"
            },
            {
                "cost": "325.00",
                "description": "Nightly cost for a group campsite with 30–39 people in the party. All rates subject to applicable taxes and fees.",
                "title": "Group (30–39 People)"
            },
            {
                "cost": "400.00",
                "description": "Nightly cost for a group campsite with 40–49 people in the party. All rates subject to applicable taxes and fees.",
                "title": "Group (40–49 People)"
            },
            {
                "cost": "475.00",
                "description": "Nightly cost for a group campsite with 50–60 people in the party. All rates subject to applicable taxes and fees.",
                "title": "Group (50–60 People)"
            }
        ],
        "directionsOverview": "Located on the Grand Loop Road just a few miles southwest of Lake Village and next to the Bridge Bay Marina.",
        "directionsUrl": "https://www.nps.gov/yell/planyourvisit/directions.htm",
        "operatingHours": [
            {
                "exceptions": [
                    {
                        "exceptionHours": {
                            "wednesday": "Closed",
                            "monday": "Closed",
                            "thursday": "Closed",
                            "sunday": "Closed",
                            "tuesday": "Closed",
                            "friday": "Closed",
                            "saturday": "Closed"
                        },
                        "startDate": "2022-09-05",
                        "name": "Closed",
                        "endDate": "2023-05-18"
                    }
                ],
                "description": "The campground is only open during summer. Checkout time is 11 am. See exceptions for exact dates of operation.",
                "standardHours": {
                    "wednesday": "All Day",
                    "monday": "All Day",
                    "thursday": "All Day",
                    "sunday": "All Day",
                    "tuesday": "All Day",
                    "friday": "All Day",
                    "saturday": "All Day"
                },
                "name": "Bridge Bay Campground"
            }
        ],
        "addresses": [
            {
                "postalCode": "82190",
                "city": "Yellowstone National Park",
                "stateCode": "WY",
                "line1": "GPS Coordinates: N 44 32.070 W 110 26.218",
                "type": "Physical",
                "line3": "",
                "line2": ""
            },
            {
                "postalCode": "82190",
                "city": "Yellowstone National Park",
                "stateCode": "WY",
                "line1": "PO Box 165",
                "type": "Mailing",
                "line3": "",
                "line2": ""
            }
        ],
        "images": [
            {
                "credit": "NPS/Renkin",
                "crops": [],
                "title": "Bridge Bay Campground",
                "altText": "Sign",
                "caption": "Bridge Bay Campground is located close to the Bridge Bay Marina and Yellowstone Lake.",
                "url": "https://www.nps.gov/common/uploads/structured_data/3C85386A-1DD8-B71B-0B318D416ECCFD35.jpg"
            },
            {
                "credit": "NPS/Renkin",
                "crops": [],
                "title": "Bridge Bay Campground",
                "altText": "Tents at campsite",
                "caption": "The campground location encompasses woods and meadows and some sites look out on Yellowstone Lake.",
                "url": "https://www.nps.gov/common/uploads/structured_data/3C853A1A-1DD8-B71B-0B2A18034CDE6EBD.jpg"
            },
            {
                "credit": "NPS/Renkin",
                "crops": [],
                "title": "Bridge Bay Campground",
                "altText": "Tents at campground site",
                "caption": "Bison are frequent visitors at Bridge Bay Campground.",
                "url": "https://www.nps.gov/common/uploads/structured_data/3C853DF1-1DD8-B71B-0BB6DC9B887F4329.jpg"
            },
            {
                "credit": "NPS/Renkin",
                "crops": [],
                "title": "Bridge Bay Campground",
                "altText": "Tent at campground",
                "caption": "Bridge Bay Campground.",
                "url": "https://www.nps.gov/common/uploads/structured_data/3C853F08-1DD8-B71B-0B67CE5771B270E0.jpg"
            },
            {
                "credit": "NPS/Renkin",
                "crops": [],
                "title": "Bridge Bay Campground",
                "altText": "Campsite in trees",
                "caption": "Bridge Bay Campground offers camping sites in the trees and meadows.",
                "url": "https://www.nps.gov/common/uploads/structured_data/3C854075-1DD8-B71B-0BF7B6854F406BED.jpg"
            },
            {
                "credit": "NPS/Renkin",
                "crops": [],
                "title": "Bridge Bay Campground",
                "altText": "RV at campsite",
                "caption": "Bridge Bay Campground",
                "url": "https://www.nps.gov/common/uploads/structured_data/3C861666-1DD8-B71B-0B13290665426F19.jpg"
            },
            {
                "credit": "NPS/Renkin",
                "crops": [],
                "title": "Bridge Bay Campground",
                "altText": "RV at campsite",
                "caption": "Bridge Bay Campground",
                "url": "https://www.nps.gov/common/uploads/structured_data/3C8617F3-1DD8-B71B-0B539F8A422D4345.jpg"
            },
            {
                "credit": "NPS/Renkin",
                "crops": [],
                "title": "Bridge Bay Campground",
                "altText": "Tent at campsite",
                "caption": "Bridge Bay Campground",
                "url": "https://www.nps.gov/common/uploads/structured_data/3C86192D-1DD8-B71B-0B0A7223F4A57936.jpg"
            }
        ],
        "weatherOverview": "Yellowstone's weather can vary greatly within a day and throughout the park. Snow can fall every month of the year. Summer highs reach 70–80°F (25–30°C) during the day, but nights can be cool (and it may freeze at higher elevations). During spring and fall, daytime highs range from 30–60°F (0–20°C), with overnight lows below freezing.",
        "numberOfSitesReservable": "432",
        "numberOfSitesFirstComeFirstServe": "0",
        "campsites": {
            "totalSites": "432",
            "group": "4",
            "horse": "0",
            "tentOnly": "0",
            "electricalHookups": "0",
            "rvOnly": "0",
            "walkBoatTo": "0",
            "other": "0"
        },
        "accessibility": {
            "wheelchairAccess": "Paths in the campground area are compacted gravel/dirt with limited gradient change.",
            "internetInfo": "",
            "cellPhoneInfo": "There is a cell phone tower near Lake Village: check with your provider to verify coverage. The number of people in the busy summer months of July and August can overwhelm cellular circuits resulting in an inability to send or receive a call, text, or data.",
            "fireStovePolicy": "Wood and charcoal fires are permitted in this campground, though special fire restrictions are occasionally put in place when the danger of wildland fires is great. If you plan to light a fire in the park, please ask about current fire restrictions at the entrance station when you arrive. Propane grills and stoves are normally not restricted.",
            "rvAllowed": "1",
            "rvInfo": "Campground sites that will accommodate a maximum combined length of 40-feet or more are limited. Most campsites in Yellowstone will not accommodate oversize units. Call Yellowstone National Park Lodges at 307-344-7311 (307-344-5395 for TDD services) for more information. When calling to make a reservation, be prepared to give the size of your tent (in feet) or the combined length of your RV and any other vehicles or towed vehicles.",
            "rvMaxLength": "0",
            "additionalInfo": "Showers not included.",
            "trailerMaxLength": "0",
            "adaInfo": "Three accessible campsites. Accessible restrooms are located in Loop A of campground and in the picnic area along the entrance road to Bridge Bay Marina.",
            "trailerAllowed": "1",
            "accessRoads": [
                "Paved Roads - All vehicles OK"
            ],
            "classifications": [
                "Developed Campground"
            ]
        },
        "multimedia": [],
        "lastIndexedDate": ""
    },
    {
        "id": "127199AC-A753-4B49-B3FF-1F8484B61CBE",
        "url": "https://www.nps.gov/bibe/planyourvisit/basin_campground.htm",
        "name": "Chisos Basin Campground",
        "parkCode": "bibe",
        "description": "The Chisos Basin Campground is nestled in an open woodland within a scenic mountain basin. Campers enjoy the iconic views of Casa Grande and Emory Peak. The sunset through the nearby \"Window\" is a Big Bend highlight. Some of the park's most popular trails begin nearby. Elevation is 5,400 feet. RESERVATIONS REQUIRED. Trailers over 20 feet and RV's over 24 feet are not recommended due to the narrow, winding road to the Basin and small campsites.",
        "latitude": "29.275900792177",
        "longitude": "-103.302285692397",
        "latLong": "{lat:29.275900792177, lng:-103.302285692397}",
        "audioDescription": "The Chisos Basin Campground is nestled in an open woodland within a scenic mountain basin. Campers enjoy spectacular views of Casa Grande and Emory Peak. Sunset through the nearby \"Window\" is a Big Bend highlight. Some of the park's most popular trails begin nearby. RESERVATIONS REQUIRED. Trailers over 20 feet and RV's over 24 feet are not recommended due to the narrow, winding road to the Basin and small campsites.",
        "isPassportStampLocation": "0",
        "passportStampLocationDescription": "",
        "passportStampImages": [],
        "geometryPoiId": "9C6AD356-E629-4713-9C3D-0B61E34EB27B",
        "reservationInfo": "Reservations are required. No first-come, first-serve camping. The Chisos Basin Campground is open year-round. -- 2/3 of campsites are reservable up to 6 months in advance. -- 1/3 of campsites are reservable up to 14 days in advance. Reservations for Chisos Basin Campground are made at: https://www.recreation.gov/camping/campgrounds/234038",
        "reservationUrl": "https://www.recreation.gov/camping/campgrounds/234038",
        "regulationsurl": "https://www.nps.gov/bibe/planyourvisit/campground_rules.htm",
        "regulationsOverview": "Visitors may stay in the park up to 14 consecutive nights, with a limit of 28 nights in a calendar year. During the busiest time of the year, January 1 through April 15, visitors are limited to 14 total nights in the park. Pets are permitted in the campground, but must be leashed at all times. Leashes must be six feet (1.8 m) or less in length. Pets may not be left unattended for their safety and for the safety of other visitors and wildlife. Pets are NOT allowed on trails. Wood and ground fires are prohibited. All food items must be stored inside a vehicle or animal-proof food storage locker. Generators allowed in specified sites only 8:00 AM-11:00 AM and 5:00 PM-8:00 PM",
        "amenities": {
            "trashRecyclingCollection": "Yes - year round",
            "toilets": [
                "Flush Toilets - year round"
            ],
            "internetConnectivity": "No",
            "showers": [
                "None"
            ],
            "cellPhoneReception": "Yes - year round",
            "laundry": "No",
            "amphitheater": "Yes - year round",
            "dumpStation": "Yes - year round",
            "campStore": "Yes - year round",
            "staffOrVolunteerHostOnsite": "Yes - year round",
            "potableWater": [
                "Yes - year round"
            ],
            "iceAvailableForSale": "Yes - year round",
            "firewoodForSale": "No",
            "foodStorageLockers": "Yes - year round"
        },
        "contacts": {
            "phoneNumbers": [],
            "emailAddresses": [
                {
                    "description": "",
                    "emailAddress": "BIBE_info@nps.gov"
                }
            ]
        },
        "fees": [
            {
                "cost": "16.00",
                "description": "Per night. Campsite capacity is maximum of 8 people and two vehicles. Reservations required through www.recreation.gov.\n\nPark entrance fees are separate from campground fees. An entrance pass is required in the park and while staying in the campground.",
                "title": "Campsite Fee"
            }
        ],
        "directionsOverview": "Several highways lead to Big Bend National Park: TX 118 from Alpine to Study Butte or FM 170 from Presidio to Study Butte (then 26 miles east to park headquarters), or US 90 or US 385 to Marathon (then 70 miles south to park headquarters). The Chisos Basin Campground is located in the Chisos Basin near the end of the Chisos Basin Road, 10 miles from Panther Junction.",
        "directionsUrl": "https://www.nps.gov/bibe/planyourvisit/directions.htm",
        "operatingHours": [
            {
                "exceptions": [],
                "description": "Open year round.",
                "standardHours": {
                    "wednesday": "All Day",
                    "monday": "All Day",
                    "thursday": "All Day",
                    "sunday": "All Day",
                    "tuesday": "All Day",
                    "friday": "All Day",
                    "saturday": "All Day"
                },
                "name": "Chisos Basin Campground"
            }
        ],
        "addresses": [],
        "images": [
            {
                "credit": "NPS",
                "crops": [],
                "title": "View of Casa Grande",
                "altText": "View of Casa Grande",
                "caption": "View of Casa Grande from the campground",
                "url": "https://www.nps.gov/common/uploads/structured_data/50EF6BFA-F15F-7BE0-0B6EBAB89E4F0927.jpg"
            },
            {
                "credit": "NPS",
                "crops": [],
                "title": "Chisos Basin Campground",
                "altText": "Chisos Basin Campground",
                "caption": "Nice views from the campsites in the Chisos Basin Campground",
                "url": "https://www.nps.gov/common/uploads/structured_data/21C2D79E-E1B0-73E2-AD7990C9EB6DA39F.jpg"
            },
            {
                "credit": "NPS",
                "crops": [],
                "title": "View of the Window",
                "altText": "View of the Window from Basin campsite",
                "caption": "A nice view of the Window from a Basin campsite",
                "url": "https://www.nps.gov/common/uploads/structured_data/5116FB0E-922E-BCA8-62FF078FAA4F37BE.jpg"
            },
            {
                "credit": "NPS",
                "crops": [],
                "title": "Basin campground area",
                "altText": "Basin campground area",
                "caption": "Basin campground area",
                "url": "https://www.nps.gov/common/uploads/structured_data/8E03C3B9-94E9-AABF-AE3FAEF7D0D253CF.jpg"
            }
        ],
        "weatherOverview": "",
        "numberOfSitesReservable": "56",
        "numberOfSitesFirstComeFirstServe": "0",
        "campsites": {
            "totalSites": "56",
            "group": "7",
            "horse": "0",
            "tentOnly": "0",
            "electricalHookups": "0",
            "rvOnly": "0",
            "walkBoatTo": "0",
            "other": "0"
        },
        "accessibility": {
            "wheelchairAccess": "The campground is somewhat hilly, but paved roads connect campsite parking areas and restrooms.",
            "internetInfo": "",
            "cellPhoneInfo": "There is cell service in the campground area.",
            "fireStovePolicy": "No fires. Charcoal grills only.",
            "rvAllowed": "1",
            "rvInfo": "RVs and Trailers over 24' are NOT recommended.",
            "rvMaxLength": "24",
            "additionalInfo": "Trailers over 20' and RVs over 24' are not recommended due to the narrow, winding road into the Basin and the very small campsites in the campground. There are very few places where a larger recreational vehicle can turn around within the entire Chisos Basin area.",
            "trailerMaxLength": "20",
            "adaInfo": "One ADA campsite (site #37) with paved walkway, accessible picnic table and grill. Adjacent to water spigot and restroom facility.",
            "trailerAllowed": "1",
            "accessRoads": [
                "Paved Roads - All vehicles OK"
            ],
            "classifications": [
                "Developed Campground"
            ]
        },
        "multimedia": [],
        "lastIndexedDate": ""
    }
]

exports.data = data
import './App.css';
import About from './pages/About'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from './pages/HomePage'
import Campground_Model from './pages/Campground_Model'
import Trails_Model from './pages/Trails_Model'
import Parks_Model from './pages/Parks_Model'
import NavBar from './navbar/navbar.js'
import ParksPage from './pages/ParksPage';
import TrailsPage from './pages/TrailsPage';
import CampgroundsPage from './pages/CampgroundsPage';
import Search from './pages/Search';
import DevVisualizations from './components/DeveloperVisualizations/visualizations';
import OurVisualizations from './components/OurVisualizations/vis';

function App() {
  return (
    <div className="App">
      <NavBar/>
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/about" element={<About />} />
        <Route path="/parks" element={<Parks_Model />} />
        <Route path="/parks/:id" element={<ParksPage />} />
        <Route path="/trails" element={<Trails_Model />} />
        <Route path="/trails/:id" element={<TrailsPage />} />
        <Route path="/campgrounds" element={<Campground_Model />} />
        <Route path="/campgrounds/:id" element={<CampgroundsPage />} />
        <Route path="/devVisualizations" element={<DevVisualizations />} />
        <Route path="/ourVisualizations" element={<OurVisualizations />} />
		<Route path="/search/:query" element={<Search />} />
      </Routes>
    </BrowserRouter>

    </div>
  );
}

export default App;

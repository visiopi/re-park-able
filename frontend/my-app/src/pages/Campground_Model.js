
import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import CampgroundCard from "../Cards/CampgroundCard";
import Pagination from "react-bootstrap/Pagination";
import "./ModelPageFormatting.css";
import OneSlider from "../components/OneSlider"
import Container from "react-bootstrap/Container"
import Form from "react-bootstrap/Form"
import DropDown from "../components/DropDown"
import Button from "react-bootstrap/Button"

var queryReg = null;

const client = axios.create({
    baseURL: "https://api.re-park-able.me/"
});

function Campground_Model() {
	const searchQuery = useRef("");

    const [campgrounds, setCampgrounds] = useState([]);
    const [loaded, setLoaded] = useState(false);
    const [page, setPage] = useState(1);

    const [sort, setSort] = useState("sort");
    const [sortOrder, setSortOrder] = useState("");
    const [feeMax, setFeeMax] = useState(60);
    const [internetFilter, setInternetFilter] = useState("noFilter");

    function changePage(pageNum) {
        setPage(pageNum);
        setLoaded(false);
    }

    const handleFeeMax = (value) => {
        setFeeMax(value);
        setLoaded(false);
    }

    const handleSortFilter = (value) => {
		setSort(value);
        setLoaded(false);
	};

    const handleInternetFilter = (value) => {
        let internetStatus = "noFilter";
        if (value == "Yes") {
            internetStatus = "1";
        } else if (value == "No") {
            internetStatus = "0";
        } else {
            internetStatus = "noFilter";
        }
        setInternetFilter(internetStatus);
        setLoaded(false);
    }

    const handleSortOrder = (value) => {
        setSortOrder(value=="Descending"?"dec":"");
        setLoaded(false);
    }

    useEffect(() => {
        const fetchCampgrounds = async() => {
            if(!loaded) {
                var query = `campgrounds?page=${page}&perPage=20`
                if(searchQuery.current.value != "") {
                    query=`search/campground/${searchQuery.current.value}`;
					queryReg = new RegExp(
						`(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
						"i"
					  );
			
                } else {
					queryReg = null;
                    if(sort != "sort") {
                        query+="&sort=" + sort
                        query+=
                        sortOrder=="dec"?"&sortOrder=dec":"";
                    }
                    
                    if(internetFilter != "noFilter") {
                        query+="&internet=" + internetFilter;
                    }
    
                    query+="&fees="+feeMax;
                }

                console.log( "https://api.re-park-able.me/" + query)
                await client
                    .get(query)
                    .then((response) => {
                        setCampgrounds(response.data);
                    })
                    .catch((err) => console.log(err));
                setLoaded(true);
            }
        };
        fetchCampgrounds();
    }, [campgrounds, loaded]);

    var totalPages = 100; //requires fix
    var pagination_items = [];

    for(let i = page-2; i<page+3; i++) {
        if(i<=0 || i> totalPages) {
            continue;
        }
        pagination_items.push(
            <Pagination.Item
                key={i}
                onClick={() => changePage(i)}
                active={i==page}
            >{i}</Pagination.Item>
        );
    }

    return (
        <div>
		<br></br>
            <h1 class= "model-title">Campgrounds</h1>
			<br></br>
			<Form
				onSubmit={(event) => {
				event.preventDefault();
				setLoaded(false);
				}}
				className="d-flex pb-2 justify-content-center"
			>
				<Form.Control
				ref={searchQuery}
				style={{ width: "20vw" }}
				type="search"
				placeholder="Search campgrounds"
				className="me-2"
				aria-label="Search"
				/>
				<Button variant="outline-secondary" onClick={() => setLoaded(false)}>
					Search
				</Button>
			</Form>
			<Container>
				<Form>
                    <Row>
                        <Col>
                            <DropDown 
                                title = "Based On"
                                items = {["name", "fees", "parkCode", "totalSites"]}
                                onChange={handleSortFilter}
                            />
                        </Col>
                        <Col>
                            <DropDown 
                                title = "Sort"
                                items = {["Ascending", "Descending"]}
                                onChange={handleSortOrder}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <h4>Fees (in dollars)</h4>
                    </Row>
                    <Row>
                        <Col>
                            <OneSlider
                                min={0}
                                max={60}
                                onChange={handleFeeMax}
                                discrete
                            />
                        </Col>
                    </Row>
                    <Row>
                        <h4>Internet availability</h4>
                    </Row>
                    <Row>
                        <DropDown 
                            title = "Choose an option:"
                            items = {["Yes", "No", "Doesn't matter"]}
                            onChange={handleInternetFilter}
						/>
                    </Row>
                </Form>
            </Container>
            <Row className="pad-cards">
                {loaded && 
                    campgrounds["data"].map((campground) => {
                        return (
                            <Col>
                                <CampgroundCard campground={campground} regex={queryReg} />
                            </Col>
                        );
                    })
                }
            </Row>
            <Pagination>
                {page >3 && (
                    <Pagination.Item
                        first
                        key={1}
                        onClick={() => changePage(1)}
                        active={1==page}
                    >1</Pagination.Item>
                )}
                {page > 4 && <Pagination.Ellipsis />}
                {pagination_items}
                {page < totalPages - 3 && <Pagination.Ellipsis />}
                {page < totalPages - 2 && (
                    <Pagination.Item
                        last
                        key={totalPages}
                        onClick={() => changePage(totalPages)}
                        active={totalPages === page}
                    >{totalPages}</Pagination.Item>
                )}
            </Pagination>
        </div>
      
    );
  }
  
  export default Campground_Model;
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import React from "react";
import {Highlight} from 'react-highlight-regex';

const CampgroundCard = (props) => {
    const {fees, id, image, internet, name, parkCode, totalSites} = props.campground || [];

	function highlightText(input) {
		if (props.regex != null) {
		  return <Highlight match={props.regex} text={input} />;
		}
		return input;
	  }
	
    return (
		<Link to={"/campgrounds/" + id} style={{ textDecoration: 'none', color: 'black'}}>
        <Card style={{ width: '15rem'}} data-testid="campground-card" className="card h-100">
            <Card.Body>
                <Card.Title>{highlightText(name)}</Card.Title>
                <Card.Img variant = "top" src={image} style={{ objectFit: 'cover', height: '200px' }}/>
                <Card.Text>{highlightText("Fees: $" + fees)}</Card.Text>
                <Card.Text>{highlightText("Number of sites reservable: " + totalSites)}</Card.Text>
                <Card.Text>{highlightText("Internet Access: " + internet)}</Card.Text>
                <Card.Text>{highlightText("Park: " + parkCode)}</Card.Text>
                <Button variant="success">Learn More</Button>
                
            </Card.Body>
        </Card>
		</Link>

    );
    
  }
  
  export default CampgroundCard;
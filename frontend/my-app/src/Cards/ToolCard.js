import React from "react"

import Card from "react-bootstrap/Card"
import Button from  "react-bootstrap/Button"
import Container from "react-bootstrap/Container"

const ToolCard = (props) => {
    const {name, image, description, link} = props.toolInfo || [];
    return (
	<Card style={{ width: '18rem' }} data-testid="tool-card">
		<Card.Img variant="top" src={image} />
		<Card.Body>
			<Card.Title>{name}</Card.Title>
			<Card.Text>{description}</Card.Text>
		</Card.Body>
		<Card.Footer>
			<Button href={link} variant="success">Learn More</Button>
		</Card.Footer>
	</Card>
        
    );
};

export default ToolCard;
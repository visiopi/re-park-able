import React from "react";
import { BrowserRouter } from "react-router-dom";
import renderer from "react-test-renderer";

import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";


import ToolCard from '../Cards/ToolCard';
import APICard from '../Cards/APICard';
import MemberCard from '../Cards/MemberCard';
import CampgroundCard from '../Cards/CampgroundCard';
import TrailCard from '../Cards/TrailCard';
import ParkCard from '../Cards/ParkCard';
import NavBar from "../navbar/navbar";

import About from '../pages/About';
import HomePage from '../pages/HomePage';
import App from '../App';
import Campground_Model from "../pages/Campground_Model";
import Park_Model from "../pages/Parks_Model"
import Trails_Model from "../pages/Parks_Model";

// Test 1: Make sure the ToolCard for the about page is being rendered
test("Testing ToolCard", () => {
	render(<ToolCard />, { wrapper: BrowserRouter });
	expect(screen.getByTestId("tool-card")).toBeInTheDocument();
  });

// Test 2: Make sure the APICard for the about page is being rendered
test("Testing APICard", () => {
	render(<APICard />, { wrapper: BrowserRouter });
	expect(screen.getByTestId("api-card")).toBeInTheDocument();
  });

// Test 3: Make sure the TrailCard for the about page is being rendered
test("Testing TrailCard", () => {
	render(<TrailCard />, { wrapper: BrowserRouter });
	expect(screen.getByTestId("trail-card")).toBeInTheDocument();
  });

// Test 4: Make sure the CampgroundCard for the about page is being rendered
test("Testing CampgroundCard", () => {
	render(<CampgroundCard />, { wrapper: BrowserRouter });
	expect(screen.getByTestId("campground-card")).toBeInTheDocument();
  });

// Test 5: Make sure the ParkCard for the about page is being rendered
test("Testing ParkCard", () => {
	render(<ParkCard />, { wrapper: BrowserRouter });
	expect(screen.getByTestId("park-card")).toBeInTheDocument();
  });

// Test 6: Make sure the MemberCard for the about page is being rendered
test("Testing MemberCard", () => {
	render(<MemberCard />, { wrapper: BrowserRouter });
	expect(screen.getByTestId("member-card")).toBeInTheDocument();
  });
  
// Test 7: Make sure the app itself initializes correctly
it("App", () => {
	const component = renderer.create(<App />);

	let tree = component.toJSON();
	expect(tree).toMatchSnapshot();
});

// Test 8: Make sure the About Page being created
test("Testing About Page appearence", () => {
	render(<About />, { wrapper: BrowserRouter });
	expect(screen.getByTestId("about-page")).toBeInTheDocument();
  });

// Test 9: Make sure the About page headers are being created
test("Testing About Page elements", () => {
	render(<About />, { wrapper: BrowserRouter });
	expect(screen.getByText("Tools Used")).toBeInTheDocument();
	expect(screen.getByText("Re-Park-Able API Documentation")).toBeInTheDocument();
	expect(screen.getByText("Meet the Team!")).toBeInTheDocument();
	expect(screen.getByText("Click here to see how we built this website!")).toBeInTheDocument();
  });

  // Test 10: Make sure the Home page headers are being created
test("Testing Home Page elements", () => {
	render(<HomePage />, { wrapper: BrowserRouter });
	expect(screen.getByText("National Parks")).toBeInTheDocument();
	expect(screen.getByText("Find a beautiful park!")).toBeInTheDocument();
	expect(screen.getByText("Find a Park")).toBeInTheDocument();

	expect(screen.getByText("Hiking Trails")).toBeInTheDocument();
	expect(screen.getByText("Find a hiking trail!")).toBeInTheDocument();
	expect(screen.getByText("Find a Trail")).toBeInTheDocument();

	expect(screen.getByText("Campgrounds")).toBeInTheDocument();
	expect(screen.getByText("Find a campground!")).toBeInTheDocument();
	expect(screen.getByText("Find a Campground")).toBeInTheDocument();
	
  });

/* TESTING SORTING FILTERING AND SEARCHING */

// Test 11: Search component showing up in campgrounds model
test("Testing Search Bar in Campgrounds Model", () => {
	render(<Campground_Model />);
    expect(screen.getByPlaceholderText('Search campgrounds')).toBeInTheDocument();
});

// Test 12: Filter Components in the trails model
test("Testing Dropdowns in the Trails Page", () => {
	render(<Trails_Model />);
	expect(screen.getByText('Based On')).toBeInTheDocument();
	expect(screen.getByText('Sort')).toBeInTheDocument();
});

// Test 13: Filter Components in the parks model
test("Testing Filters in the Parks Page", () => {
	render(<Park_Model />);
	expect(screen.getByText('Based On')).toBeInTheDocument();
	expect(screen.getByText('Sort')).toBeInTheDocument();
	expect(screen.getByText('State')).toBeInTheDocument();
	expect(screen.getByText('Fees (in dollars)')).toBeInTheDocument();

});

// Test 14: Search wide search
test("Testing Search Wide Search", () => {
	render(<Park_Model />);
	expect(screen.getByText('Search')).toBeInTheDocument();

});

  
  





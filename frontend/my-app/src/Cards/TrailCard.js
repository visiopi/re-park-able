import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import {Highlight} from 'react-highlight-regex';

const TrailCard = (props) => {
    const {location, image, duration, activityType, petsPermitted, parkCode, id, feesApply, reservationRequired} = props.trail || [];

	function highlightText(input) {
		if (props.regex != null) {
		  return <Highlight match={props.regex} text={input} />;
		}
		return input;
	  }
    
    let petsPermittedString = petsPermitted ? "Yes" : "No";

    let durationWithUnits = duration + " minutes";
    if (!duration) {
        durationWithUnits = "No information provided"
    }

    return (
		<Link to={"/trails/" + id} style={{ textDecoration: 'none', color: 'black'}}>
        <Card style={{ width: '22rem'}} data-testid="trail-card" className="card h-100">
            <Card.Body>
                <Card.Title>{highlightText(location)}</Card.Title>
                <Card.Img variant = "top" src={image} style={{ objectFit: 'cover', height: '200px'}}/>
                <Card.Text>{highlightText("Duration: " + durationWithUnits)}</Card.Text>
                <Card.Text>{highlightText("Popular Activity: " + activityType)}</Card.Text>
                <Card.Text>{highlightText("Pets Allowed: " + petsPermittedString)}</Card.Text>
                <Card.Text>{highlightText("Park: " + parkCode)}</Card.Text>
				<Card.Text>{highlightText("Fees Applied: " + feesApply)}</Card.Text>
				<Card.Text>{highlightText("Reservation Required: " + reservationRequired)}</Card.Text>
                <Button variant="success">Learn More</Button>
                
            </Card.Body>
        </Card>
		</Link>
    );
  }
  
  export default TrailCard;
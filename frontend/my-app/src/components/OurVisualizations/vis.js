import TrailDurationChart from "./TrailDurationChart";
import StateCountChart from "./StateCountChart";
import ParkPriceChart from "./ParkPriceChart";
import CampgroundFees from "./CampGroundFees";

const OurVisualizations = () => {
    return (
        <div>
            <TrailDurationChart /> 
            <StateCountChart /> 
            <ParkPriceChart />
            <CampgroundFees />
        </div>
    );
}

export default OurVisualizations;
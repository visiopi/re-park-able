import Ayushi from '../images/ayushi_headshot.png'
import Zara from '../images/zara_headshot.png'
import Regina from '../images/regina_headshot.png'
import Raksheet from '../images/raksheet_headshot.png'
import Ashwanth from '../images/ashwanth_headshot.png'

const teamInfo = [
        {
            name: "Zara Shipchandler",
            gitlabName: "Zara A Shipchandler",
            altGitlabName: "Zara A Shipchandler",
            gitlabID: "@z.shipchandler",
            image: Zara,
            role: "Backend",
            bio: "I am a second year computer science and math major at UT Austin. In my free time, I love doing art, working out, and trying new foods- both around Austin and in the kitchen!",
            commits: 0,
            issues: 0,
            unitTests: 7
        },
        {
            name: "Ayushi Oswal",
            gitlabName: "Ayushi Oswal",
            altGitlabName: "Ayushi Oswal",
            gitlabID: "@ayushi.oswal",
            image: Ayushi,
            role: "Front-end",
            bio: " I am a second-year Computer Science at UT Austin and am also pursing a minor in Business. I enjoy painting and spending time with my friends.",
            commits: 0,
            issues: 0,
            unitTests: 10
        },
        {
            name: "Regina Ye",
            gitlabName: "visiopi",
            altGitlabName: "Regina Ye",
            gitlabID: "@visiopi",
            image: Regina,
            role: "Front-end",
            bio: " I am a computer science student at UT Austin. I like drawing, cooking, and watching movies.",
            commits: 0,
            issues: 0,
            unitTests: 0
        },
        {
            name: "Raksheet Kota",
            gitlabName: "Raksheet Kota",
            altGitlabName: "eragon1248",
            gitlabID: "@raksheetkota",
            image: Raksheet,
            role: "Front-end/Backend",
            bio: " I am a second year student at UT Austin majoring in Computer Science and Mathematics. In my free time I enjoy visiting local coffee shops and hanging out with friends.",
            commits: 0,
            issues: 0,
            unitTests: 7
        },
        {
            name: "Ashwanth Muruhathasan",
            gitlabName: "Ashwanth Muruhathasan",
            altGitlabName: "Ashwanth Muruhathasan",
            gitlabID: "@ashwanth2001",
            image: Ashwanth,
            role: "Front-end",
            bio: " I am a senior at UT Austin majoring in Computer Science, Finance, and Mathematics. In my free time, I enjoy playing basketball, watching every show on Netflix, and playing poker. ",
            commits: 0,
            issues: 0,
            unitTests: 10
        }
    ]

export {teamInfo};
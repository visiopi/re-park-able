from flask_marshmallow import Marshmallow
from models import Park, Campground, Trail
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

ma = Marshmallow()


class ParkSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Park


class CampgroundSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Campground


class TrailSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Trail


park_schema = ParkSchema()
campground_schema = CampgroundSchema()
trail_schema = TrailSchema()

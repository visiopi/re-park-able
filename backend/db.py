import json
from models import app, db, Park, Campground, Trail
import re

def standardizeDurations(str):
    if not str:
        return None
    units = {'Minute': 1, 'Hour': 60, 'Day': 1440}
    for unit in units:
        if unit in str:
            numbers = re.findall(r'\d+', str)
            avg = sum([int(num) for num in numbers])/len(numbers)
            return avg*units[unit]

def populate():
    populate_parks()
    populate_trails()
    populate_campgrounds()

def populate_parks():
    test_data = open("data/parks_data.json", encoding="utf8")
    test_dict = json.load(test_data)
    test_data.close()
    parks = test_dict["data"]
    for park in parks:
        phone = "Not available"
        if park["contacts"]["phoneNumbers"]:
            phone = park["contacts"]["phoneNumbers"][0]["phoneNumber"]

        entranceFee = None
        if park["entranceFees"]:
            entranceFee = float(park["entranceFees"][0]["cost"])

        db_row = {
            "parkCode": park["parkCode"],
            "url": park["url"],
            "image": park["images"][0]["url"],
            "name": park["fullName"],
            "description": park["description"],
            "state": park["addresses"][0]["stateCode"],
            "latitude": float(park["latitude"]) if park["latitude"] else None,
            "longitude": float(park["longitude"]) if park["longitude"] else None,
            "activities": ", ".join(
                [activity["name"] for activity in park["activities"]]
            ),
            "weather": park["weatherInfo"],
            "phone": phone,
            "email": park["contacts"]["emailAddresses"][0]["emailAddress"],
            "weather": park["weatherInfo"],
            "entranceFee": entranceFee,
            "directions": park["directionsInfo"],
        }
        db.session.add(Park(**db_row))
    db.session.commit()

def populate_trails():
    test_data = open("data/trails_data.json", encoding="utf8")
    test_dict = json.load(test_data)
    test_data.close()
    trails = test_dict["data"]
    for trail in trails:
        parkCode = "Not available"
        if trail["relatedParks"]:
            parkCode = trail["relatedParks"][0]["parkCode"]

        db_row = {
            "id": trail["id"],
            "url": trail["url"],
            "title": trail["title"],
            "description": trail["shortDescription"],
            "image": trail["images"][0]["url"],
            "parkCode": parkCode,
            "tags": ", ".join([tag for tag in trail["tags"]]),
            "latitude": float(trail["latitude"]) if trail["latitude"] else None,
            "longitude": float(trail["longitude"]) if trail["longitude"] else None,
            "location": trail["location"],
            "accessibility": trail["accessibilityInformation"],
            "reservationRequired": trail["isReservationRequired"] == 'true',
            "feesApply": trail["doFeesApply"] == 'true',
            "petsPermitted": trail["arePetsPermitted"] == 'true',
            "activityType": ", ".join(
                [activity["name"] for activity in trail["activities"]]
            ),
            "activityDesc": trail["activityDescription"],
            "duration":standardizeDurations(trail["duration"]),
        }
        db.session.add(Trail(**db_row))
    db.session.commit()


def populate_campgrounds():
    test_data = open("data/campgrounds_data.json", encoding="utf8")
    test_dict = json.load(test_data)
    test_data.close()
    camps = test_dict["data"]
    for camp in camps:
        image = "Not available"
        if camp["images"]:
            image = camp["images"][0]["url"]

        fees = None
        if camp["fees"]:
            fees = float(camp["fees"][0]["cost"]) if camp["fees"][0]["cost"] else None

        db_row = {
            "id": camp["id"],
            "url": camp["url"],
            "name": camp["name"],
            "parkCode": camp["parkCode"],
            "description": camp["description"],
            "latitude": float(camp["latitude"]) if camp["latitude"] else None,
            "longitude": float(camp["longitude"]) if camp["longitude"] else None,
            "reservationInfo": camp["reservationInfo"],
            "regulations": camp["regulationsOverview"],
            # "amenities": camp["amenities"],
            "image": image,
            "fees": fees,
            "totalSites": int(camp["campsites"]["totalSites"]),
            "internet": bool('Yes' in camp["amenities"]["internetConnectivity"]) if camp["amenities"]["internetConnectivity"] else None,
        }
        db.session.add(Campground(**db_row))
    db.session.commit()


if __name__ == "__main__":
    with app.app_context():
        db.drop_all()
        db.create_all()
        populate()

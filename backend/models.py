from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask_cors import CORS


app = Flask(__name__)
CORS(app)
DATABASE_URL = "mysql+pymysql://admin:parksarecool@database-1.ceigoovlnnme.us-east-2.rds.amazonaws.com:3306/Test_DB"
app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URL
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
#TODO hide db password

class Park(db.Model):
    parkCode = db.Column(db.String(10), primary_key=True)
    url = db.Column(db.String(100))
    image = db.Column(db.String(100))
    name = db.Column(db.String(100))
    description = db.Column(db.String(1000))
    state = db.Column(db.String(10))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    activities = db.Column(db.String(1000))
    phone = db.Column(db.String(1000))
    email = db.Column(db.String(1000))
    weather = db.Column(db.String(100))
    entranceFee = db.Column(db.Float)
    directions = db.Column(db.String(100))


class Trail(db.Model):
    id = db.Column(db.String(100), primary_key=True)
    url = db.Column(db.String(100))
    title = db.Column(db.String(100))
    description = db.Column(db.String(1000))
    image = db.Column(db.String(100))
    parkCode = db.Column(db.String(10))
    tags = db.Column(db.String(100))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    location = db.Column(db.String(100))
    accessibility = db.Column(db.String(1000))
    reservationRequired = db.Column(db.Boolean)
    feesApply = db.Column(db.Boolean)
    petsPermitted = db.Column(db.Boolean)
    activityType = db.Column(db.String(100))
    activityDesc = db.Column(db.String(1000))
    duration = db.Column(db.Float)


class Campground(db.Model):
    id = db.Column(db.String(100), primary_key=True)
    url = db.Column(db.String(100))
    name = db.Column(db.String(100))
    parkCode = db.Column(db.String(10))
    description = db.Column(db.String(1000))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    reservationInfo = db.Column(db.String(100))
    regulations = db.Column(db.String(100))
    amenities = db.Column(db.String(100))
    image = db.Column(db.String(100))
    fees = db.Column(db.Float)
    totalSites = db.Column(db.Integer)
    internet = db.Column(db.Boolean)


@app.route("/")
def hello_world():
    return "Hello, World!"

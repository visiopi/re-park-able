import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.re-park-able.me/"
#URL = "http://localhost:3000/"

class Test(unittest.TestCase):

  @classmethod
  def setUpClass(self) -> None:
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    options.add_argument("--headless")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-dev-shm-usage")
    chrome_prefs = {}
    options.experimental_options["prefs"] = chrome_prefs
    # Disable images
    chrome_prefs["profile.default_content_settings"] = {"images": 2}

    self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
    self.driver.maximize_window()
    self.driver.get(URL)

  @classmethod
  def tearDownClass(self):
    self.driver.quit()

  def test_parks(self):
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
    element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
    element.click()
    # Wait for splash card to load, then click
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, 'Find a Park')))
    element = self.driver.find_element(By.LINK_TEXT, 'Find a Park')
    self.driver.execute_script("arguments[0].click();", element)
    self.assertEqual(self.driver.current_url, URL + "parks")

  def test_trails(self):
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
    element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
    element.click()
    # Wait for splash card to load, then click
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, 'Find a Trail')))
    element = self.driver.find_element(By.LINK_TEXT, 'Find a Trail')
    self.driver.execute_script("arguments[0].click();", element)
    self.assertEqual(self.driver.current_url, URL + "trails")

  def test_campgrounds(self):
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
    element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
    element.click()
    # Wait for splash card to load, then click
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, 'Find a Campground')))
    element = self.driver.find_element(By.LINK_TEXT, 'Find a Campground')
    self.driver.execute_script("arguments[0].click();", element)
    self.assertEqual(self.driver.current_url, URL + "campgrounds")

  def test_nav_brand(self):
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
    element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
    element.click()
    self.assertEqual(self.driver.current_url, URL)

  def test_home_URL(self):
    extension = ""
    self.driver.get(URL + extension)
    self.assertEqual(self.driver.current_url, URL + extension)

  def test_about_URL(self):
    extension = "about"
    self.driver.get(URL + extension)
    self.assertEqual(self.driver.current_url, URL + extension)

  def test_parks_URL(self):
    extension = "parks"
    self.driver.get(URL + extension)
    self.assertEqual(self.driver.current_url, URL + extension)

  def test_trails_URL(self):
    extension = "trails"
    self.driver.get(URL + extension)
    self.assertEqual(self.driver.current_url, URL + extension)

  def test_campgrounds_URL(self):
    extension = "campgrounds"
    self.driver.get(URL + extension)
    self.assertEqual(self.driver.current_url, URL + extension)

  def test_loaded(self):
    extension = "parks"
    self.driver.get(URL + extension)
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn.btn-success')))

    extension = "trails"
    self.driver.get(URL + extension)
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn.btn-success')))

    extension = "campgrounds"
    self.driver.get(URL + extension)
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn.btn-success')))



if __name__ == '__main__':
    unittest.main()

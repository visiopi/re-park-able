# Re-Park-Able



## Group Info
### IDB Group 08
### Group Information
| Name                  | EID     | GitLab          |
| --------------------- | ------- | --------------- |
| Zara Shipchandler     | zas747  | z.shipchandler  |
| Ayushi Oswal          | ao26386 | ayushi.oswal    |
| Raksheet Kota         | rrk789  | raksheetkota    |
| Regina Ye             | ryy77   | visiopi         |
| Ashwanth Muruhathasan | am92446 | ashwanth2001    |

Project Leader: Raksheet Kota. 
Tasks: Assign tasks to all members, do final check of rubric, submit project link.

## Project Information
**Website Name:** Re-Park-Able 

**Link:** https://www.re-park-able.me/

**GitLab Pipeline:** https://gitlab.com/visiopi/re-park-able/-/pipelines/845207565

**Git SHA** 1156cdb0f4192202727c01c15e6efdd58c5de0b1

## Estimated Time of Completion
| Name                  | Estimated  | Actual     |
| --------------------- | ---------- | ---------- |
| Zara Shipchandler     | 5 hours   | 7 hours   |
| Ayushi Oswal          | 4 hours   | 5 hours   |
| Raksheet Kota         | 3 hours   | 4 hours   |
| Regina Ye             | 5 hours   | 5 hours   |
| Ashwanth Muruhathasan | 4 hours   | 4 hours   |

## Comments
Got code for .gitlab-ci.yml from IDB Group 19 from Spring 2023 and GeoJobs from Spring 2022.

Got code for searching implementation details from GeoJobs from Spring 2022.

import React from 'react';
import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';
import './PagesFormatting.css';
import GoogleMap from './GoogleMap';

const client = axios.create({
    baseURL: "https://api.re-park-able.me/"
});

const TrailsPage = () => {
    const { id } = useParams();
    const [trail, setTrail] = useState();

    useEffect(() => {
        const fetchTrail = async () => {
            if (trail === undefined) {
                var query = "trail/" + id;
                console.log("https://api.re-park-able.me/" + query);
                await client
                    .get(query)
                    .then((response) => {
                        setTrail(response.data.data);
                    })
                    .catch((err) => console.log(err));
            }
        }
        fetchTrail();
    }, [trail]);

    if (trail === undefined) {
        return <div className="App">Loading...</div>;
    }

    let trailDuration = trail.duration + "minutes";
    if (!trail.duration) {
        trailDuration = "No information provided"
    }

    return (
        <div>
            <h1 class="header-style">
                {trail.location}
            </h1>
            <body>
                <img src={trail.image} width="300" height="300"></img>
                <div class="body-text">
                    <h5>
                        Description:
                    </h5>
                    <p>
                        {trail.description}
                    </p>
                    <div class="row">
                        <div class ="col-md-6">
                            <h5>
                                Main activity: 
                            </h5>
                            <p>
                                {trail.activityType}
                            </p>
                            <h5>
                                Park: 
                            </h5>
                            <p>
                                {trail.park}
                            </p>       
                        </div>     
                        <div class ="col-md-6">
                            <h5>
                                Pets allowed: 
                            </h5>        
                            <p>
                                {trail.petsPermitted && "Yes"}
                                {!trail.petsPermitted && "No"}
                            </p>
                            <h5>
                                Trail duration:  
                            </h5>
                            <p>
                                {trailDuration}
                            </p>
                        </div>     
                    </div>
                    <div class="row gx-5">
                        <GoogleMap place={trail} name={trail.location}/>
                    </div>
                </div>
            </body>
            <Link to={"/parks/" + trail.parkCode}>
                <button>Park for trail</button>
            </Link>
            <Link to={"/campgrounds/" + trail.campground}>
                <button>Campground near this trail</button>
            </Link>
        </div>
    )
}

export default TrailsPage
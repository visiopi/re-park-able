 import React from 'react'
import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import "./PagesFormatting.css";
import "./ModelPageFormatting.css";

const client = axios.create({
    baseURL: "https://api.re-park-able.me/"
});

const ParksPage = () => {
    const { id } = useParams();
    const [park, setPark] = useState();

    useEffect(() => {
        const fetchPark = async () => {
            if (park === undefined) {
                var query = "park/" + id;
                console.log("https://api.re-park-able.me/" + query);
                await client
                    .get(query)
                    .then((response) => {
                        setPark(response.data.data);
                    })
                    .catch((err) => console.log(err));
            }
        }
        fetchPark();
    }, [park]);

    if (park === undefined) {
        return <div className="App">Loading...</div>;
    }

    return (
        <div>
            <h1 class="header-style">
                {park.name}
            </h1>
            <body>
                <img src={park.image} width="300" height="300"></img>
                <div class="body-text">
                    <p>
                        Description: {park.description}
                    </p>
                    <div class="row">
                        <div class ="col-md-6">
                            <p>
                                URL: <a href={park.url} target='_blank'>{park.url}</a>
                            </p>
                        </div>
                        <div class ="col-md-6">
                            <p>
                                Park code: {park.parkCode}
                            </p>
                            <p>
                                Latitude and longitude: {park.latitude + " Lat, " + park.longitude + " Lon"}
                            </p>
                            <p>
                                State: {park.state}
                            </p>
                        </div>
                    </div>
					<div>
						<iframe
							title="map"
							className="map"
							loading="lazy"
							allowFullScreen
							referrerPolicy="no-referrer-when-downgrade"
							width="100%"
							height="300"
							src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyAa0pVSA26KxyWPdzXupgd8-OTjlsq_Rvc&q=${park.name}`}
						></iframe>
					</div>
					

                </div>
            </body>
            <Link to={"/campgrounds/" + park.campground}>
                <Button variant="success">Campground for this park</Button>
            </Link>
            <Link to={"/trails/" + park.trail}>
                <Button variant="success">Trail in this park</Button>
            </Link>
        </div>
    )
}

export default ParksPage
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import {Highlight} from 'react-highlight-regex';

const ParkCard = (props) => {
    const {name, image, entranceFee, state, activities, parkCode} = props.park || [];

	function highlightText(input) {
		if (props.regex != null) {
		  return <Highlight match={props.regex} text={input} />;
		}
		return input;
	  }

    return (
		<Link to={"/parks/" + parkCode} style={{ textDecoration: 'none', color: 'black'}}>
        <Card style={{ width: '15rem'}} data-testid="park-card" className="card h-100">
            <Card.Body>
                <Card.Title>{highlightText(name)}</Card.Title>
                <Card.Img variant = "top" src={image} style={{ objectFit: 'cover', height: '200px'}}/>
                <Card.Text>{highlightText("Fees: $" + entranceFee)}</Card.Text>
                <Card.Text>{highlightText("State: " + state)}</Card.Text>
                <Card.Text>{highlightText("Activities include: " + activities)}</Card.Text>
				<Card.Text>{highlightText("Park Code: " + parkCode)}</Card.Text>
                
                    <Button variant="success">Learn More</Button>
            </Card.Body>
        </Card>  
		</Link>    
    );
  }
  
  export default ParkCard;
import React from "react"

import Card from "react-bootstrap/Card"
import Button from  "react-bootstrap/Button"
import { Container } from "react-bootstrap";

const APICard = (props) => {
    const {name, image, description, link} = props.apiInfo  || [];
    return (
		<Card style={{ width: '18rem' }} data-testid="api-card">
			<Card.Img variant="top" src={image} />
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Text>{description}</Card.Text>
			</Card.Body>
			<Card.Footer>
				<Button href={link} variant="success">Learn More</Button>
			</Card.Footer>
		</Card>
        
    );
};

export default APICard;
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button';
import Container from "react-bootstrap/Container";
import { useState, useEffect } from 'react';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Stack from 'react-bootstrap/Stack'

import ToolCard from '../Cards/ToolCard'
import APICard from '../Cards/APICard'
import MemberCard from '../Cards/MemberCard'
import {toolInfo, apiInfo} from '../data/toolsInfo'

import { teamInfo } from '../data/teamInfo';


export default function About() {

    const [loadedCommits, setLoadedC] = useState(false);
    const [loadedIssues, setLoadedI] = useState(false);
    const [teamList, setTeamList] = useState([]);
    const [totalCommits, setTotalCommits] = useState(0);
    const [totalIssues, setTotalIssues] = useState(0);

    const [issues, setIssues] = useState([]);
    const [commits, setCommits] = useState([{ name: "", commits: "" }]);

    async function getCommitInfo() {
        fetch(`https://gitlab.com/api/v4/projects/43353652/repository/contributors`).then((response) => response.json()).then((data) => {
            setTotalCommits(0);
            var commits = 0;
            data.forEach((item) => {
                teamInfo.forEach((member) => {
                    if (member.gitlabName === item.name || member.name === item.name || member.altGitlabName === item.name) {
                        member.commits += item.commits;
                        commits += item.commits;
                    }
                })

            })
            setTotalCommits(commits);
            setTeamList(teamInfo);
            setLoadedC(true); 
        })
    }

    async function getIssuesInfo() {
        fetch(`https://gitlab.com/api/v4/projects/43353652/issues`).then((response) => response.json()).then((data) => {
            setTotalIssues(0);
            var issues = 0;
            data.forEach((item) => {
                let found = false;
                teamInfo.forEach((member) => {
                    if (item.assignee != null) {
                        if (member.gitlabName === item.assignee.name || member.altGitlabName === item.assignee.name) {
                            member.issues += 1;
                            issues += 1
                            found = true
                        }
                    } 
                })
                if (!found) {
                    teamInfo.forEach((member) => {
                        if (item.closed_by != null) {
                            if (member.gitlabName === item.closed_by.name) {
                                member.issues += 1;
                                issues += 1
                            }
                        }
                    })

                }
            })
            setTotalIssues(issues);
            setTeamList(teamInfo);
            setLoadedI(true);
            
        })
    }

    function getTotalTest() {
        var result = 0;
        teamInfo.forEach((member) => {
            result += member.unitTests;
        })
        return result
    }

    useEffect(() => {
        getIssuesInfo();
        getCommitInfo();
    }, [])

    return (
        <Stack data-testid="about-page">
            <Container>
                <h1>About Us</h1>
                <p>Re-park-able aims to connect you to the beauty of the national parks the US has to offer. If you are interested in a national park, consult our website to see nearby parks, the activities they have to offer, and nearby campgrounds! Putting this information together in one website makes it easy for you to plan your next national park visit!</p>

				<p><a href="https://gitlab.com/visiopi/re-park-able">Click here to see how we built this website!</a></p>
            </Container>
                
            <Container>
                <h1 className="d-flex justify-content-center p-4 ">Meet the Team!</h1>
                {(loadedCommits && loadedIssues) ? (
					<div>
						<Row className="row-cols-md-5 g-3">
							{teamInfo.map((member) => {
								return (
									<Col className="d-flex align-self-stretch">
										<MemberCard teamInfo={member} />
									</Col>
								);
							})} 
						</Row>
							
						<h1>Totals</h1>
						<p>Total # Commits: {totalCommits} </p>
						<p>Total # Issues: {totalIssues} </p>
						<p>Total # Tests: {getTotalTest()} </p>
					</div>
				) : (
					<h1>Loading...</h1>
				)}
            </Container>
            
            <Container className="p-4">
                <h1>API's</h1>
                <a href="https://documenter.getpostman.com/view/25878073/2s93CExcaA">Re-Park-Able API Documentation</a>
					<br></br>
					<br></br>
					<br></br>
                <Row className="g-3">
                    {apiInfo.map((i) => {
                        return (
                            <Col className="d-flex align-self-stretch">
                                <APICard apiInfo={i} />
                            </Col>
                        );
                    })}
                </Row>
            </Container>
                
            <Container className="p-4">
				<br></br>
                <h1>Tools Used</h1>
				<br></br> 
                <Row className="g-3">
                    {toolInfo.map((i) => {
                        return (
                            <Col className="d-flex align-self-stretch">
                                <ToolCard toolInfo={i} />
                            </Col>
                        );
                    })} 
                </Row>
            </Container>
            
            
            <br></br>
        
            <br></br>
        </Stack>
    )
}
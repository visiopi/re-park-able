import React from "react"

import Card from "react-bootstrap/Card"
import Button from  "react-bootstrap/Button"

const ToolCard = (props) => {
    const {name, gitlabName, gitlabID, image, role, bio, commits, issues, unitTests} = props.teamInfo || [];

    return (
    	<Card class="card h-100" data-testid="member-card">
			<Card.Img variant = "top" src={image}/>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				
				<Card.Subtitle>{"Role: " + role}</Card.Subtitle>
				<Card.Text>{"Gitlab ID: " + gitlabID}</Card.Text>
				<Card.Text>{"About me: " + bio}</Card.Text>
				<Card.Text>
					{"# Commits: " + commits}
				</Card.Text>
				<Card.Text>{"# Issues: " + issues}</Card.Text>
				<Card.Text>{"# Unit Tests: " + unitTests}</Card.Text>
			</Card.Body>
		</Card>
    );
};

export default ToolCard;
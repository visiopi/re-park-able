import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Cell, Treemap, Rectangle, Legend, Tooltip, ResponsiveContainer } from 'recharts';
import { useState, useEffect } from "react";
import axios from "axios";

const COLORS = ['#8A9A5B', '#2E8B57', '#9FE2BF', '#009E60', '#00FF7F'];

const STATES = [
	'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID',
	'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS',
	'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK',
	'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV',
	'WI', 'WY'
]

const PRE_DATA = [
	{ name: 'CA', value: 30 },
	{ name: 'NY', value: 26 },
	{ name: 'NM', value: 23 },
	{ name: 'VA', value: 23 },
	{ name: 'MD', value: 21 },
	{ name: 'AZ', value: 20 },
	{ name: 'AK', value: 19 },
	{ name: 'PA', value: 18 },
	{ name: 'MA', value: 15 },
	{ name: 'TX', value: 15 },
	{ name: 'CO', value: 13 },
	{ name: 'UT', value: 12 },
	{ name: 'WA', value: 11 },
	{ name: 'FL', value: 10 },
	{ name: 'GA', value: 10 },
	{ name: 'HI', value: 9 },
	{ name: 'AL', value: 8 },
	{ name: 'NC', value: 8 },
	{ name: 'OH', value: 8 },
	{ name: 'SC', value: 8 },
	{ name: 'AR', value: 7 },
	{ name: 'MS', value: 7 },
	{ name: 'MO', value: 7 },
	{ name: 'TN', value: 7 },
	{ name: 'MI', value: 6 },
	{ name: 'SD', value: 6 },
	{ name: 'WY', value: 6 },
	{ name: 'ID', value: 5 },
	{ name: 'KS', value: 5 },
	{ name: 'KY', value: 5 },
	{ name: 'ME', value: 5 },
	{ name: 'NE', value: 5 },
	{ name: 'NJ', value: 5 },
	{ name: 'WV', value: 5 },
	{ name: 'LA', value: 4 },
	{ name: 'MN', value: 4 },
	{ name: 'MT', value: 4 },
	{ name: 'OR', value: 4 },
	{ name: 'IN', value: 3 },
	{ name: 'NV', value: 3 },
	{ name: 'ND', value: 3 },
	{ name: 'OK', value: 3 },
	{ name: 'RI', value: 3 },
	{ name: 'WI', value: 3 },
	{ name: 'CT', value: 2 },
	{ name: 'IL', value: 2 },
	{ name: 'IA', value: 2 },
	{ name: 'DE', value: 1 },
	{ name: 'NH', value: 1 },
	{ name: 'VT', value: 1 }
  ]


const baseURL = "https://api.re-park-able.me/parks"

let data = [];

const StateCountChart = () => {
    const [loading, setLoading] = useState(true)
	const [localData, setLocalData] = useState([]);

	// get data for each category
	const getData = async () => {
		/*let innerData = []
		for(let i = 0; i<STATES.length; i++) {
			let state = STATES[i];
			var query = `?state=` + state;
			let newURL = baseURL + query;
			const response = await axios.get(newURL);
			let responseData = response.data.data;
			innerData.push({name: state, value: responseData.length});
		}
		setLocalData(innerData);*/
		setLoading(false);
	}

	const getParks = () => {
		
		data = PRE_DATA;
		//data = localData;
		return data;
	}

	useEffect(() => {
        getData();
    }, []);

	const CustomTooltip = ({ active, payload, label }) => {
		if (active && payload && payload.length) {
		  return (
			<div className="treemap-custom-tooltip">
			  <p>{` ${payload[0].payload.name} : ${payload[0].value} `}</p>
			</div>
		  );
		}
	  
		return null;
	};


	return (
		<div>
		{loading && <p>Loading...</p>}
		{!loading && <Container fluid="md">
			<Row style={{ width: "100%", height: 600 }}>
				<h3 className="p-5 text-center">Number of Parks in each State</h3>
				<Col>
					<ResponsiveContainer width="100%" height="100%">
						<Treemap
              				data={getParks()}
							dataKey="value"
							ratio={4 / 3}
							stroke="#fff"
							fill="#8A9A5B"
							isAnimationActive={false}
            			>
							<Tooltip content={<CustomTooltip />}/>
						</Treemap>
					</ResponsiveContainer>
				</Col>
			</Row>
		</Container>}
		</div>
	);
};

export default StateCountChart;
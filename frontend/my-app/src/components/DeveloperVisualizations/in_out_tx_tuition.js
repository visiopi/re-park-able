import React from 'react';
import { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const InOutTuition = () => {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://api.campuseatsandtreats.me/colleges?state=TX')
        .then (res => res.json())
        .then (res => {
            setData(res.results);
            setLoading(false);});
    }, []);

    return (
        <div>
        {loading && <p>Loading...</p>}
        {!loading && <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">In-State vs. Out-of-state Tuition for TX Colleges</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                    <BarChart
                        height={300}
                        data={data}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="in_state_tuition" fill="#8884d8" unit="$"/>
                        <Bar dataKey="out_of_state_tuition" fill="#ffc658" unit="$"/>
                    </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>}
        </div>
    );
}

export default InOutTuition;


from flask import jsonify, request, Response
from models import app, db, Park, Campground, Trail
from schema import park_schema, campground_schema, trail_schema
from sqlalchemy.sql import text, column, desc
from sqlalchemy import or_
import json

DEFAULT_PAGE_SIZE = 20


@app.route("/parks")
def get_parks():
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    name = request.args.get("name")
    state = request.args.get("state")
    activity = request.args.get("activity")
    entrancefee = request.args.get("entrancefee")
    parkcode = request.args.get("parkcode")
    sort = request.args.get("sort")
    sortOrder = request.args.get("sortOrder")

    query = db.session.query(Park)

    #Filter
    if name is not None:
        query = query.filter(Park.name == name)

    if state is not None:
        query = query.filter(Park.state == state)

    if activity is not None:
        query = query.filter(Park.activities.contains(activity))

    if entrancefee is not None:
        query = query.filter(Park.entranceFee <= entrancefee)

    if parkcode is not None:
        query = query.filter(Park.parkCode == parkcode)

    #Sort
    if sort is not None and getattr(Park, sort) is not None:
        if sortOrder is not None and sortOrder == "dec":
            query = query.order_by(desc(getattr(Park, sort)))
        else:
            query = query.order_by(getattr(Park, sort))
            
    
    count = query.count()
    if page is not None:
        query = paginate(query, page, perPage)
    result = park_schema.dump(query, many=True)
    return jsonify({"data": result, "meta": {"count": count}})


@app.route("/park/<string:id>")
def get_park(id):
    query = db.session.query(Park).filter_by(parkCode=id)
    try:
        result = park_schema.dump(query, many=True)[0]
    except IndexError:
        return 404
    queryTrail = db.session.query(Trail).filter_by(parkCode=id)
    try:
        trail = trail_schema.dump(queryTrail, many=True)[0]["id"]
    except IndexError:
        trail = "None"
    queryCampground = db.session.query(Campground).filter_by(parkCode=id)
    try:
        campground = campground_schema.dump(queryCampground, many=True)[0]["id"]
    except IndexError:
        campground = "None"
    result.update({"trail": trail})
    result.update({"campground": campground})
    return jsonify({"data": result})


@app.route("/trails")
def get_trails():
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    title = request.args.get("title")
    reservationRequired = request.args.get("reservationRequired")
    feesApply = request.args.get("feesApply")
    petsPermitted = request.args.get("petsPermitted")
    activityType = request.args.get("activityType")
    durationLowerBound = request.args.get("durationLowerBound")
    durationUpperBound = request.args.get("durationUpperBound")
    sort = request.args.get("sort")
    sortOrder = request.args.get("sortOrder")

    query = db.session.query(Trail)

    #Filter
    if title is not None:
        query = query.filter(Trail.title == title)
    if reservationRequired is not None:
        query = query.filter(Trail.reservationRequired == reservationRequired)
    if feesApply is not None:
        query = query.filter(Trail.feesApply == feesApply)
    if petsPermitted is not None:
        query = query.filter(Trail.petsPermitted == petsPermitted)
    if activityType is not None:
        query = query.filter(Trail.activityType.contains(activityType))
    if durationLowerBound and durationUpperBound is not None:
        query = query.filter(Trail.duration >= durationLowerBound, Trail.duration <= durationUpperBound)

    #Sort
    if sort is not None and getattr(Trail, sort) is not None:
        if sortOrder is not None and sortOrder == "dec":
            query = query.order_by(desc(getattr(Trail, sort)))
        else:
            query = query.order_by(getattr(Trail, sort))

    count = query.count()
    if page is not None:
        query = paginate(query, page, perPage)
    result = trail_schema.dump(query, many=True)
    return jsonify({"data": result, "meta": {"count": count}})


@app.route("/trail/<string:id>")
def get_trail(id):
    query = db.session.query(Trail).filter_by(id=id)
    try:
        result = trail_schema.dump(query, many=True)[0]
    except IndexError:
        return 404
    queryPark = db.session.query(Trail).filter_by(parkCode=result["parkCode"])
    try:
        park = park_schema.dump(queryPark, many=True)[0]["parkCode"]
    except IndexError:
        park = "None"
    queryCampground = db.session.query(Campground).filter_by(
        parkCode=result["parkCode"]
    )
    try:
        campground = campground_schema.dump(queryCampground, many=True)[0]["id"]
    except IndexError:
        campground = "None"
    result.update({"park": park})
    result.update({"campground": campground})
    return jsonify({"data": result})


@app.route("/campgrounds")
def get_campgrounds():
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    query = db.session.query(Campground)
    name = request.args.get("name")
    fees = request.args.get("fees")
    totalSites = request.args.get("totalSites")
    internet = request.args.get("internet")
    parkCode = request.args.get("parkCode") 
    sort = request.args.get("sort")
    sortOrder = request.args.get("sortOrder")

    #Filter
    if name is not None:
        query = query.filter(Campground.name == name)
    if fees is not None:
        query = query.filter(Campground.fees <= fees)
    if totalSites is not None:
        query = query.filter(Campground.totalSites <= totalSites)
    if internet is not None:
        query = query.filter(Campground.internet == internet)
    if parkCode is not None:
        query = query.filter(Campground.parkCode == parkCode)

    #Sort
    if sort is not None and getattr(Campground, sort) is not None:
        if sortOrder is not None and sortOrder == "dec":
            query = query.order_by(desc(getattr(Campground, sort)))
        else:
            query = query.order_by(getattr(Campground, sort))

    count = query.count()
    if page is not None:
        query = paginate(query, page, perPage)
    result = campground_schema.dump(query, many=True)
    return jsonify({"data": result, "meta": {"count": count}})


@app.route("/campground/<string:id>")
def get_campground(id):
    query = db.session.query(Campground).filter_by(id=id)
    try:
        result = campground_schema.dump(query, many=True)[0]
    except IndexError:
        return 404
    queryPark = db.session.query(Campground).filter_by(parkCode=result["parkCode"])
    try:
        park = park_schema.dump(queryPark, many=True)[0]["parkCode"]
    except IndexError:
        park = "None"
    queryTrail = db.session.query(Trail).filter_by(parkCode=result["parkCode"])
    try:
        trail = campground_schema.dump(queryTrail, many=True)[0]["id"]
    except IndexError:
        trail = "None"
    result.update({"park": park})
    result.update({"trail": trail})
    return jsonify({"data": result})

@app.route("/search/<string:model>/<string:query>")
def search_models(model, query): 
    model = model.strip().lower()
    terms = query.split()
    result = None
    if model == "park":
        occurrences = search_parks(terms)
        parks = sorted(occurrences.keys(), 
            key = lambda x: occurrences[x],
            reverse = True)
        result = park_schema.dump(parks, many=True)
    elif model == "trail":
        occurrences = search_trails(terms)
        trails = sorted(occurrences.keys(),
            key = lambda x: occurrences[x],
            reverse = True)
        result = trail_schema.dump(trails, many=True)
    elif model == "campground":
        occurrences = search_campgrounds(terms)
        campgrounds = sorted(occurrences.keys(),
            key = lambda x: occurrences[x],
            reverse = True)
        result = campground_schema.dump(campgrounds, many=True)
    elif model == "all":
        occurrences = {
        **search_parks(terms),
        **search_trails(terms),
        **search_campgrounds(terms)
        }
        objs = sorted(occurrences.keys(), 
            key = lambda x: occurrences[x],
            reverse = True)
        parks = [park for park in objs if type(park) == Park]
        trails = [trail for trail in objs if type(trail) == Trail]
        campgrounds = [campground for campground in objs if type(campground) == Campground]
        park_results = park_schema.dump(parks, many=True)
        trail_results = trail_schema.dump(trails, many=True)
        campground_results = campground_schema.dump(campgrounds, many=True)
        return jsonify({
            "parks": park_results,
            "trails": trail_results, 
            "campgrounds": campground_results
        })
    else: 
        jsonify({"Invalid model": 400})
    return jsonify({"data": result})

"""
Returns the parks corresponding to the given terms
"""
def search_parks(terms):
    occurrences = {}
    for term in terms: 
        queries = []
        queries.append(Park.parkCode.contains(term))
        queries.append(Park.url.contains(term))
        queries.append(Park.image.contains(term))
        queries.append(Park.name.contains(term))
        queries.append(Park.description.contains(term))
        queries.append(Park.state.contains(term))
        queries.append(Park.latitude.contains(term))
        queries.append(Park.longitude.contains(term))
        queries.append(Park.activities.contains(term))
        queries.append(Park.phone.contains(term))
        queries.append(Park.email.contains(term))
        queries.append(Park.weather.contains(term))
        queries.append(Park.entranceFee.contains(term))
        queries.append(Park.directions.contains(term))
        parks =  Park.query.filter(or_(*queries))
        for park in parks:
            if not park in occurrences:
                occurrences[park] = 1
            else:
                occurrences[park] += 1
    return occurrences

"""
Returns the trails corresponding to the given terms
"""
def search_trails(terms):
    occurrences = {}
    for term in terms: 
        queries = []
        queries.append(Trail.url.contains(term))
        queries.append(Trail.title.contains(term))
        queries.append(Trail.description.contains(term))
        queries.append(Trail.image.contains(term))
        queries.append(Trail.latitude.contains(term))
        queries.append(Trail.longitude.contains(term))
        queries.append(Trail.location.contains(term))
        queries.append(Trail.accessibility.contains(term))
        queries.append(Trail.reservationRequired.contains(term))
        queries.append(Trail.feesApply.contains(term))
        queries.append(Trail.petsPermitted.contains(term))
        queries.append(Trail.activityType.contains(term))
        queries.append(Trail.activityDesc.contains(term))
        queries.append(Trail.duration.contains(term))
        trails =  Trail.query.filter(or_(*queries))
        for trail in trails:
            if not trail in occurrences:
                occurrences[trail] = 1
            else:
                occurrences[trail] += 1
    return occurrences
"""
Returns the campgrounds corresponding to the given terms
"""
def search_campgrounds(terms):
    occurrences = {}
    for term in terms: 
        queries = []
        queries.append(Campground.url.contains(term))
        queries.append(Campground.name.contains(term))
        queries.append(Campground.parkCode.contains(term))
        queries.append(Campground.description.contains(term))
        queries.append(Campground.latitude.contains(term))
        queries.append(Campground.longitude.contains(term))
        queries.append(Campground.reservationInfo.contains(term))
        queries.append(Campground.regulations.contains(term))
        queries.append(Campground.amenities.contains(term))
        queries.append(Campground.image.contains(term))
        queries.append(Campground.fees.contains(term))
        queries.append(Campground.totalSites.contains(term))
        queries.append(Campground.internet.contains(term))
        campgrounds =  Campground.query.filter(or_(*queries))
        for campground in campgrounds:
            if not campground in occurrences:
                occurrences[campground] = 1
            else:
                occurrences[campground] += 1
    return occurrences




"""
Returns a paginated query according the page number and number per page passed in.
"""


def paginate(query, page_num, per_page):
    if per_page is None:
        per_page = DEFAULT_PAGE_SIZE
    return query.paginate(page=page_num, per_page=per_page, error_out=False).items


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001, debug=True)

import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Cell, PieChart, Pie, Legend, Tooltip, ResponsiveContainer } from 'recharts';
import { useState, useEffect } from "react";
import axios from "axios";

const COLORS = ['#8A9A5B', '#2E8B57', '#9FE2BF', '#009E60', '#00FF7F'];

let data = [
    {
        name: "0-30 minutes",
        count: 0
    },
    {
        name: "30-60 minutes",
        count : 0
    },
    {
        name: "60-120 minutes",
        count : 0
    },
    {
        name: "120-180 minutes",
        count : 0
    },
	{
        name: "180+ minutes",
        count : 0
    },

];


const TrailDurationChart = () => {
    const [loading, setLoading] = useState(true)
	const [catOne, setCatOne] = useState([]);
	const [catTwo, setCatTwo] = useState([]);
	const [catThree, setCatThree] = useState([]);
	const [catFour, setCatFour] = useState([]);
	const [catFive, setCatFive] = useState([]);

	const baseUrl = "https://api.re-park-able.me/trails?";

	// get data for each category
	const getData = async () => {
		// 0 - 30 minutes
		let newURL = baseUrl + "durationLowerBound=-1&durationUpperBound=31";
		const responseOne = await axios.get(newURL);
		let responseTrailDataOne = responseOne.data.data;

		// 30 - 60 minutes
		newURL = baseUrl + "durationLowerBound=30&durationUpperBound=61";
		const responseTwo = await axios.get(newURL);
		let responseTrailDataTwo = responseTwo.data.data;
		
		// 60-120 minutes
		newURL = baseUrl + "durationLowerBound=60&durationUpperBound=121";
		const responseThree = await axios.get(newURL);
		let responseTrailDataThree = responseThree.data.data;

		// 120 -180 minutes
		newURL = baseUrl + "durationLowerBound=120&durationUpperBound=181";
		const responseFour = await axios.get(newURL);
		let responseTrailDataFour = responseFour.data.data;

		// 180+ minutes
		newURL = baseUrl + "durationLowerBound=180&durationUpperBound=13000";
		const responseFive = await axios.get(newURL);
		let responseTrailDataFive = responseFive.data.data;

		// setting the arrays for corresponding arrays with the right trails 
		setCatOne(responseTrailDataOne);
		setCatTwo(responseTrailDataTwo);
		setCatThree(responseTrailDataThree);
		setCatFour(responseTrailDataFour);
		setCatFive(responseTrailDataFive);
		setLoading(false);
	}

	const getTrails = () => {
		data = [
			{
				name: "0-30 minutes",
				count: 135
			},
			{
				name: "30-60 minutes",
				count : 16
			},
			{
				name: "60-120 minutes",
				count : 1
			},
			{
				name: "120-180 minutes",
				count : 3
			},
			{
				name: "180+ minutes",
				count : 3
			},
		];

		data[0]['count'] = catOne.length;
		data[1]['count'] = catTwo.length;
		data[2]['count'] = catThree.length;
		data[3]['count'] = catFour.length;
		data[4]['count'] = catFive.length;

		return data;
	}

	useEffect(() => {
        getData();
    }, []);

	return (
		<div>
		{loading && <p>Loading...</p>}
		{!loading && <Container fluid="md">
			<Row style={{ width: "100%", height: 600 }}>
				<h3 className="p-5 text-center">Number of Trails of Different Durations</h3>
				<Col>
					<ResponsiveContainer width="100%" height="100%">
						<PieChart width={400} height={400}>
							<Pie
								dataKey="count"
								isAnimationActive={false}
								data={getTrails()}
								cx="50%"
								cy="50%"
								outerRadius={200}
								fill="#8884d8"
								label
							>
							{data.map((entry, index) => (
							<Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
							))}
							</Pie>
							<Tooltip />
						</PieChart>
					</ResponsiveContainer>
				</Col>
			</Row>
		</Container>}
		</div>
	);
};

export default TrailDurationChart;
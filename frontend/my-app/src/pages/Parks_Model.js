import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ParkCard from "../Cards/ParkCard";
import Pagination from "react-bootstrap/Pagination";
import DropDown from "../components/DropDown"
import OneSlider from "../components/OneSlider"
import Container from "react-bootstrap/Container"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"

var queryReg = null;

const client = axios.create({
    baseURL: "https://api.re-park-able.me/"
});

function Park_Model() {
    const [parks, setParks] = useState([]);
    const [loaded, setLoaded] = useState(false);
    const [page, setPage] = useState(1);

	const [sort, setSort] = useState("sort");
    const [sortOrder, setSortOrder] = useState("");
    const [stateFilter, setStateFilter] = useState("State");
    const [feeMax, setFeeMax] = useState(200);

	const searchQuery = useRef("");

    function changePage(pageNum) {
        setPage(pageNum);
        setLoaded(false);
    }

	const handleSortFilter = (value) => {
		setSort(value);
        setLoaded(false);
	};

    const handleSortOrder = (value) => {
        setSortOrder(value=="Descending"?"dec":"");
        setLoaded(false);
    }

    const handleStateFilter = (value) => {
        setStateFilter(value);
        setLoaded(false);
    }

    const handleFeeMax = (value) => {
        setFeeMax(value);
        setLoaded(false);
    }
    
    useEffect(() => {
        const fetchParks = async() => {
            if(!loaded) {
                var query = `parks?page=${page}&perPage=20`
                if(searchQuery.current.value != "") {
                    query=`search/park/${searchQuery.current.value}`;
					queryReg = new RegExp(
						`(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
						"i"
					  );
                } else {
					queryReg = null;
                    if(sort != "sort") {
                        query+="&sort=" + sort
                        query+=
                        sortOrder=="dec"?"&sortOrder=dec":"";
                    }
                    
                    if(stateFilter != "State") {
                        query+="&state=" + stateFilter;
                    }

                    query+="&entrancefee="+feeMax;
                }
                
                console.log( "https://api.re-park-able.me/" + query)
                await client
                    .get(query)
                    .then((response) => {
                        setParks(response.data);
                    })
                    .catch((err) => console.log(err));
                setLoaded(true);
            }
        };
        fetchParks();
    }, [parks, loaded, feeMax]);

    var totalPages = 100; //requires fix
    var pagination_items = [];

    for(let i = page-2; i<page+3; i++) {
        if(i<=0 || i> totalPages) {
            continue;
        }
        pagination_items.push(
            <Pagination.Item
                key={i}
                onClick={() => changePage(i)}
                active={i==page}
            >{i}</Pagination.Item>
        );
    }

    return (
        <div>
		<br></br>
            <h1 class="model-title">Parks</h1>

			<Form
				onSubmit={(event) => {
				event.preventDefault();
				setLoaded(false);
				}}
				className="d-flex pb-2 justify-content-center"
			>
				<Form.Control
				ref={searchQuery}
				style={{ width: "20vw" }}
				type="search"
				placeholder="Search parks"
				className="me-2"
				aria-label="Search"
				/>
				<Button variant="outline-secondary" onClick={() => setLoaded(false)}>
					Search
				</Button>
			</Form>

			<br></br>
			<Container>
				<Form>
					<Row>
						<Col>
							<DropDown 
								title = "Based On"
								items = {["name", "entranceFee", "parkCode", "activities"]}
								onChange={handleSortFilter}
							/>
						</Col>
						<Col>
							<DropDown 
								title = "Sort"
								items = {["Ascending", "Descending"]}
								onChange={handleSortOrder}
							/>
						</Col>
                        <Col>
							<DropDown 
								title = "State"
								items={[
                                    'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID',
                                    'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS',
                                    'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK',
                                    'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV',
                                    'WI', 'WY'
                                  ]}
                                scroll
								onChange={handleStateFilter}
							/>
						</Col>
					</Row>
                    <Row className="p-3">
                        <h4>Fees (in dollars)</h4>
                    </Row>
                    <Row>
                        <Col>
                            <OneSlider
                                min={0}
                                max={200}
                                onChange={handleFeeMax}
                                discrete
                            />
                        </Col>
                    </Row>
				</Form>
			</Container>
            <Row className="pad-cards">
                {loaded && 
                    parks["data"].map((park) => {
                        return (
                            <Col>
                                <ParkCard park={park} regex={queryReg}/>
                            </Col>
                        );
                    })
                }
            </Row>
            <Pagination>
                {page >3 && (
                    <Pagination.Item
                        first
                        key={1}
                        onClick={() => changePage(1)}
                        active={1==page}
                    >1</Pagination.Item>
                )}
                {page > 4 && <Pagination.Ellipsis />}
                {pagination_items}
                {page < totalPages - 3 && <Pagination.Ellipsis />}
                {page < totalPages - 2 && (
                    <Pagination.Item
                        last
                        key={totalPages}
                        onClick={() => changePage(totalPages)}
                        active={totalPages === page}
                    >{totalPages}</Pagination.Item>
                )}
            </Pagination>
        </div>
      
    );
  }
  
  export default Park_Model;
import React from 'react'
import { useState, useEffect, useRef } from "react";
import axios from "axios";
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';
import './PagesFormatting.css';
import GoogleMap from './GoogleMap';

const client = axios.create({
    baseURL: "https://api.re-park-able.me/"
});

const CampgroundsPage = () => {
    const { id } = useParams();
    const [campground, setCampground] = useState();

    useEffect(() => {
        const fetchCampground = async () => {
            if (campground === undefined) {
                var query = "campground/" + id;
                console.log("https://api.re-park-able.me/" + query);
                await client
                    .get(query)
                    .then((response) => {
                        setCampground(response.data.data);
                    })
                    .catch((err) => console.log(err));
            }
        }
        fetchCampground();
    }, [campground]);

    if (campground === undefined) {
        return <div className="App">Loading...</div>;
    }

    return (
        <div>
            <h1 class="header-style">
                {campground.name}
            </h1>
            <body>
                <img src={campground.image} width="300" height="300"></img>
                <div class="body-text">
                    <h5>
                        Description:
                    </h5>
                    <p>
                        {campground.description}
                    </p>
                    <div class="container px-4">
                        <div class="row gx-5">
                            <div class="col">
                                <div class="p-2">
                                    <h5>
                                        Campground ID:
                                    </h5>
                                    <p>
                                        {campground.id}
                                    </p>
                                    <h5>
                                        URL:
                                    </h5>
                                    <p>
                                        {campground.url}
                                    </p>
                                    <h5>
                                        Park code:
                                    </h5>
                                    <p>
                                        {campground.parkCode}
                                    </p>
                                </div>
                            </div>
                            <div class="col">
                                <div class="p-2">
                                    <h5>
                                        Latitude and longitude:
                                    </h5>
                                    <p>
                                        {campground.latitude + " Lat, " + campground.longitude + " Lon"}
                                    </p>
                                    <h5>
                                        Reservation information:
                                    </h5>
                                    <p>
                                        {campground.reservationInfo}
                                    </p>
                                    <h5>
                                        Regulations overview: 
                                    </h5>
                                    <p>
                                        {campground.regulations}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row gx-5">
                            <GoogleMap place={campground} name={campground.name}/>
                        </div>
                    </div>
                </div>
            </body>
            <Link to={"/parks/" + campground.parkCode}>
                <button>Park for this campground</button>
            </Link>
            <Link to={"/trails/" + campground.trail}>
                <button>Trail near this campground</button>
            </Link>
        </div>
    )
}

export default CampgroundsPage


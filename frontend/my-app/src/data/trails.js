const data = [
    {
        length:"211",
        description:"The high Sierra classic named for the man who inspired many to visit. One of the most scenic trails in America.",
        name:"John Muir Trail",
        rating:"5.00",
        maps: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3176.5522405187935!2d-118.87069229999999!3d37.23460300000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8095934ed5a6db3d%3A0xe6341d7a178fe3aa!2sJohn%20Muir%20Trail!5e0!3m2!1sen!2sus!4v1676501533236!5m2!1sen!2sus",
        image: "https://www.pcta.org/wp-content/uploads/2015/12/Thousand-Island-Lake-pacific-crest-trail-5395.jpg",
        park: "Yosemite",
        difficulty: "Medium"
    },
    {
        length:"24",
        description:"The CDT runs for 24 miles from Lewis Lake and South Entrance Road to Old Faithful and US Hwy 20. Along the way, the trail passes Shoshone Lake and Geyser Basin.",
        name:"CDT WY21: Lewis Lake and South Entrance Road to Old Faithful and US Hwy 20",
        rating:"4.00",
        maps:"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5944275.297310255!2d-108.683702!3d43.3251821!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5351ed1b81592e5f%3A0x83b7c275a6822a1!2sOld%20Faithful!5e0!3m2!1sen!2sus!4v1676502012428!5m2!1sen!2sus",
        image:"https://www.calistogaspa.com/resourcefiles/attractionsnippetimages/calistogaspa-ttd-old-faithful-geyser.jpg",
        park: "Yellowstone",
        difficulty: "Hard"
    },
    {
        length:".5",
        description: "Both natural and cultural history are evident at Dugout Wells. On this easy desert stroll you will see remnants of human settlement and typical Chihuahuan Desert habitat. A shady oasis with cottonwood trees and tables at Dugout Wells provides a good area for picnicking and bird watching.",
        name: "Chihuahuan Desert Nature Trail",
        rating: "5.0",
        maps: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3480.346004935462!2d-103.13643409999999!3d29.272168400000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86f23d430803ab87%3A0xdfce3edf5a5f2af2!2sChihuahuan%20Desert%20Nature%20Trail!5e0!3m2!1sen!2sus!4v1676502072659!5m2!1sen!2sus",
        image:"https://www.nps.gov/common/uploads/cropped_image/primary/49D27572-CCDC-8B43-9BB34D3FB59EB380.jpg?width=1600&quality=90&mode=crop",
        park: "Big Bend",
        difficulty: "Easy"
    }
 ]
 
 exports.data = data
 
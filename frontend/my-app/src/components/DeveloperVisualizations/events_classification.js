import React from "react";
import { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Cell, PieChart, Pie, Legend, Tooltip, ResponsiveContainer } from 'recharts';
const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
let events = [
    {
        name: "Sports",
        count: 0
    },
    {
        name: "Music",
        count : 0
    },
    {
        name: "Arts & Theatre",
        count : 0
    },
    {
        name: "Miscellaneous",
        count : 0
    },

];

const EventsClassification = () => {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState([]);

    const getData = async () => {
        let response = await fetch('https://api.campuseatsandtreats.me/events?page=1&&per_page=100');
        response = await response.json();
        let response2 = await fetch('https://api.campuseatsandtreats.me/events?page=2&&per_page=100');
        response2 = await response2.json();
        setData(response.results.concat(response2.results));
        setLoading(false);
    }

    const getEvents = () => {
        events = [
            {
                name: "Sports",
                count: 0
            },
            {
                name: "Music",
                count : 0
            },
            {
                name: "Arts & Theatre",
                count : 0
            },
            {
                name: "Miscellaneous",
                count : 0
            },
        
        ];
        for (let i = 0; i < data.length; i++) {
            if (data[i].classification === "Sports") {
                events[0].count++;
            }
            else if (data[i].classification === "Music") {
                events[1].count++;
            }
            else if (data[i].classification === "Arts & Theatre") {
                events[2].count++;
            } else {
                events[3].count++;
            }
        }
        return events;

    }
  
  useEffect(() => {
    getData();
  }, []);

    return (
        <div>
        {loading && <p>Loading...</p>}
        {!loading && <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Number of Events in Each Classification</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="count"
                                isAnimationActive={false}
                                data={getEvents()}
                                cx="50%"
                                cy="50%"
                                outerRadius={200}
                                fill="#8884d8"
                                label
                            >
                            {events.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                            ))}
                            </Pie>
                            <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>}
        </div>
    );
}

export default EventsClassification;

import React, { useState } from "react";
import { Slider, TextField, Grid } from "@mui/material";

export default function OneSlider(props) {
    const {min, max, discrete, onChange} = props;
    const [value, setValue] = useState(max);

    const handleUpdate = (event, value) => {
        setValue(value);
        onChange(value);
    }

    const handleChange = (event) => {
        let val = event.target.value;
        if(val!="") {
            let numVal = Number(val)
            setValue(numVal);
            onChange(value);
        }
    }

    return (
        <Grid className="d-flex  gap-4">
          <TextField value={min} size="small" disabled/>
          <Slider
            value={[value]}
            onChange={handleUpdate}
            marks={discrete}
            min={min}
            max={max}
          />
          <TextField value={value} onChange={handleChange} size="small" />
        </Grid>
      );    
}
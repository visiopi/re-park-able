const toolsInfo = [
    {
        name : "React",
        image : "",
        description : "JavaScript framework for front-end development",
        link : "https://reactjs.org/f",
    },
    {
        name : "Postman",
        image : "",
        description : "Tool for designing and testing APIs",
        link : "https://www.postman.com/",
    },
    {
        name : "GitLab",
        image : "",
        description : "Git repository and CI/CD platform",
        link : "https://about.gitlab.com/",
    },
    {
        name : "Bootstrap",
        image : "",
        description : "Styling React Components",
        link : "https://react-bootstrap.github.io/",
    },
    {
        name : "NameCheap",
        image : "",
        description : "For getting our domain name",
        link : "https://www.namecheap.com/",
    },
    {
        name : "Flask",
        image : "",
        description : "A webframework for building web applications",
        link : "https://flask.palletsprojects.com/en/2.2.x/",
    },
    {
        name : "MySQL",
        image : "",
        description : "For database management",
        link : "https://www.mysql.com/",
    },
    {
        name : "AWS Amplify",
        image : "",
        description : "For web application building and hosting",
        link : "https://aws.amazon.com/amplify/",
    }   
]

export {toolsInfo}

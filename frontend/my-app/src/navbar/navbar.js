import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import './navbar.css'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';



function NavBar() {
	const submitQuery = (event) => {
		event.preventDefault()
		const form = event.currentTarget
		console.log(`search query: ${form.query.value}`)
		window.location.assign(`/search/${form.query.value}`)
	};
	

  return (
    <Navbar className="nav-bar" expand="lg" variant="dark">
      <Container>
        <Navbar.Brand href="/">Re-Park-Able</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link href="/parks">Parks</Nav.Link>
            <Nav.Link href="/trails">Trails</Nav.Link>
            <Nav.Link href="/campgrounds">Campgrounds</Nav.Link>
			<Nav.Link href="/devVisualizations">Developer Visualizations</Nav.Link>
			<Nav.Link href="/ourVisualizations">Our Visualizations</Nav.Link>
          </Nav>
		  <Form
				onSubmit={submitQuery}
				className="d-flex pb-2 justify-content-center"
			>
				<Form.Control
				style={{ width: "20vw" }}
				type="search"
				name="query"
				placeholder="Search site"
				className="me-2"
				aria-label="Search"
				/>
				<Button type = "submit" variant="outline-secondary" style={{color: 'white'}}>
					Search
				</Button>
			</Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar;
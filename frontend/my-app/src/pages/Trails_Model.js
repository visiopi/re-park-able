
import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import TrailCard from "../Cards/TrailCard";
import Pagination from "react-bootstrap/Pagination";
import "./ModelPageFormatting.css";
import OneSlider from "../components/TwoSlider"
import Container from "react-bootstrap/Container"
import Form from "react-bootstrap/Form"
import DropDown from "../components/DropDown"
import Button from "react-bootstrap/Button"

var queryReg = null;

const client = axios.create({
    baseURL: "https://api.re-park-able.me/"
});

function Trail_Model() {
	const searchQuery = useRef("");

    const [trails, setTrails] = useState([]);
    const [loaded, setLoaded] = useState(false);
    const [page, setPage] = useState(1);

	const [sort, setSort] = useState("sort");
    const [sortOrder, setSortOrder] = useState("");
	const [feeFilter, setFeeFilter] = useState("noFilter");
	const [reservationFilter, setReservationFilter] = useState("noFilter");
	const [petsFilter, setPetsFilter] = useState("noFilter");
	const [maxDuration, setMaxDuration] = useState(2160);


    function changePage(pageNum) {
        setPage(pageNum);
        setLoaded(false);
    }

	const handleMaxDuration = (value) => {
        setMaxDuration(value);
        //setLoaded(false);
    }

	const handleFeeFilter = (value) => {
        let feeStatus = "noFilter";
        if (value == "Yes") {
            feeStatus = "1";
        } else if (value == "No") {
            feeStatus = "0";
        } else {
            feeStatus = "noFilter";
        }
        setFeeFilter(feeStatus);
        setLoaded(false);
    }

	const handleReservationFilter = (value) => {
        let reservationStatus = "noFilter";
        if (value == "Yes") {
            reservationStatus = "1";
        } else if (value == "No") {
            reservationStatus = "0";
        } else {
            reservationStatus = "noFilter";
        }
        setReservationFilter(reservationStatus);
        setLoaded(false);
    }

	const handlePetsFilter = (value) => {
        let petsStatus = "noFilter";
        if (value == "Yes") {
            petsStatus = "1";
        } else if (value == "No") {
            petsStatus = "0";
        } else {
            petsStatus = "noFilter";
        }
        setPetsFilter(petsStatus);
        setLoaded(false);
    }

    const handleSortFilter = (value) => {
		setSort(value);
        setLoaded(false);
	};

	const handleSortOrder = (value) => {
        setSortOrder(value=="Descending"?"dec":"");
        setLoaded(false);
    }

    useEffect(() => {
        const fetchTrails = async() => {
            if(!loaded) {
                var query = `trails?page=${page}&perPage=20`

				if(searchQuery.current.value != "") {
                    query=`search/trail/${searchQuery.current.value}`;
					queryReg = new RegExp(
						`(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
						"i"
					  );
                } else {
					queryReg = null;
                    if(sort != "sort") {
                        query+="&sort=" + sort
                        query+=
                        sortOrder=="dec"?"&sortOrder=dec":"";
                    }

					if(feeFilter != "noFilter") {
                        query+="&feesApply=" + feeFilter;
                    }

					if(reservationFilter != "noFilter") {
                        query+="&reservationRequired=" + reservationFilter;
                    }

					if(petsFilter != "noFilter") {
                        query+="&petsPermitted=" + petsFilter;
                    }
				}

                console.log( "https://api.re-trail-able.me/" + query)
                await client
                    .get(query)
                    .then((response) => {
                        setTrails(response.data);
                    })
                    .catch((err) => console.log(err));
                setLoaded(true);
            }
        };
        fetchTrails();
    }, [trails, loaded]);

    var totalPages = 100; //requires fix
    var pagination_items = [];

    for(let i = page-2; i<page+3; i++) {
        if(i<=0 || i> totalPages) {
            continue;
        }
        pagination_items.push(
            <Pagination.Item
                key={i}
                onClick={() => changePage(i)}
                active={i==page}
            >{i}</Pagination.Item>
        );
    }

    return (
        <div>
			<br></br>
            <h1 class="model-title">Trails</h1>
			<br></br>
			<Form
				onSubmit={(event) => {
				event.preventDefault();
				setLoaded(false);
				}}
				className="d-flex pb-2 justify-content-center"
			>
				<Form.Control
				ref={searchQuery}
				style={{ width: "20vw" }}
				type="search"
				placeholder="Search trails"
				className="me-2"
				aria-label="Search"
				/>
				<Button variant="outline-secondary" onClick={() => setLoaded(false)}>
					Search
				</Button>
			</Form>
			<Container>
				<Form>
                    <Row className="g-3">
                        <Col>
                            <DropDown 
                                title = "Based On"
                                items = {["location", "parkCode", "activityType", "feesApply", "petsPermitted", "duration"]}
                                onChange={handleSortFilter}
                            />
                        </Col>
                        <Col>
                            <DropDown 
                                title = "Sort"
                                items = {["Ascending", "Descending"]}
                                onChange={handleSortOrder}
                            />
                        </Col>
						<Col>
							<DropDown 
								title = "Are Fees Okay?"
								items = {["Yes", "No", "Doesn't matter"]}
								onChange={handleFeeFilter}
							/>
						</Col>
						<Col>
							<DropDown 
								title = "Reservations Required?"
								items = {["Yes", "No", "Doesn't matter"]}
								onChange={handleReservationFilter}
							/>
						</Col>
						<Col>
							<DropDown 
								title = "Pets Allowed?"
								items = {["Yes", "No", "Doesn't matter"]}
								onChange={handlePetsFilter}
							/>
						</Col>
                    </Row>
					<br></br>

				</Form>
			</Container>
            <Row className="pad-cards">
                {loaded && 
                    trails["data"].map((trail) => {
                        return (
                            <Col>
                                <TrailCard trail={trail} regex={queryReg}/>
                            </Col>
                        );
                    })
                }
            </Row>
            <Pagination>
                {page >3 && (
                    <Pagination.Item
                        first
                        key={1}
                        onClick={() => changePage(1)}
                        active={1==page}
                    >1</Pagination.Item>
                )}
                {page > 4 && <Pagination.Ellipsis />}
                {pagination_items}
                {page < totalPages - 3 && <Pagination.Ellipsis />}
                {page < totalPages - 2 && (
                    <Pagination.Item
                        last
                        key={totalPages}
                        onClick={() => changePage(totalPages)}
                        active={totalPages === page}
                    >{totalPages}</Pagination.Item>
                )}
            </Pagination>
        </div>
      
    );
  }
  
  export default Trail_Model;
import React from "react";
import { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {ScatterChart, Scatter, XAxis, YAxis, ZAxis, CartesianGrid, Tooltip, ResponsiveContainer, Legend} from 'recharts';

  const RestaurantPriceRatingNY = () => {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState([]);

  useEffect(() => {
    fetch('https://api.campuseatsandtreats.me/restaurants?state=NY&per_page=100')
    .then (res => res.json())
    .then (res => {
        setData(res.results);
        setLoading(false);});
  }, []);

    return (
        <div>
        {loading && <p>Loading...</p>}
        {!loading && <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Restaurant Rating vs. Price in New York</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <ScatterChart
                            width={400}
                            height={400}
                            margin={{
                                top: 20,
                                right: 20,
                                bottom: 20,
                                left: 20,
                            }}
                        >
                            <CartesianGrid />
                            <XAxis type="number" dataKey="price" name="Price Scale (1 - 4)" />
                            <YAxis type="number" dataKey="rating" name="Rating" domain={[0, 5]} ticks={[0,1,2,3,4,5]} />
                            <ZAxis dataKey="name" name="Restaurant"/>
                            <Tooltip  />
                            <Scatter name="Cities" data={data} />
                        </ScatterChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>}
        </div>
    );
}

export default RestaurantPriceRatingNY;
